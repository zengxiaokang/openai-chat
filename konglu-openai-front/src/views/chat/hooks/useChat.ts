/*
 使用chat相关函数
 */
import {useChatStore, useUserStore} from '@/store'

export function useChat() {
	const chatStore = useChatStore()
	const userStore = useUserStore()

	const getChatByUuidAndIndex = (uuid: number, index: number) => {
		return chatStore.getChatByUuidAndIndex(uuid, index)
	}

	const addChat = (uuid: number, chat: Chat.Chat) => {
		chatStore.addChatByUuid(uuid, chat)
	}

	const updateChat = (uuid: number, index: number, chat: Chat.Chat) => {
		chatStore.updateChatByUuid(uuid, index, chat)
	}

	const updateChatSome = async (uuid: number, index: number, chat: Partial<Chat.Chat>) => {
		chatStore.updateChatSomeByUuid(uuid, index, chat)
		await userStore.getUserData();
	}

	return {
		addChat,
		updateChat,
		updateChatSome,
		getChatByUuidAndIndex,
	}
}

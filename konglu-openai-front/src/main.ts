import {createApp} from 'vue'
import App from './App.vue'
import {setupI18n} from './locales'
import {setupAssets, setupScrollbarStyle} from './plugins'
import {setupStore} from './store'
import {setupRouter} from './router'

async function setupApp() {
	const app = createApp(App)
	// setupAssets 样式文件
	setupAssets()

	// setupScrollbarStyle 滚动条
	setupScrollbarStyle()

	// setupI18n 国际化插件
	setupI18n(app)

	// setupStore 使用pinia
	setupStore(app)

	// setupRouter 使用路由
	await setupRouter(app)

	app.mount('#app')
}

// 注意这里千万不要用await 会阻塞渲染
setupApp().then(error => {
})

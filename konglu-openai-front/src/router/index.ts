import type {App} from 'vue'
import type {RouteRecordRaw} from 'vue-router'
import {createRouter, createWebHashHistory} from 'vue-router'
import {setupPageGuard} from './permission'
import {ChatLayout} from '@/views/chat/layout'

const routes: RouteRecordRaw[] = [
	{
		path: '/',
		name: 'Root',
		component: ChatLayout,
		redirect: '/chat',
		children: [
			{
				path: '/chat/:uuid?',
				name: 'Chat',
				component: () => import('@/views/chat/index.vue'),
			},
		],
	},

	{
		path: '/emailValidation',
		name: 'emailValidation',
		component: () => import('@/views/exception/emailValidation/index.vue'),
	},

	{
		path: '/404',
		name: '404',
		component: () => import('@/views/exception/404/index.vue'),
	},

	{
		path: '/500',
		name: '500',
		component: () => import('@/views/exception/500/index.vue'),
	},
	{
		path: '/registerResponse',
		name: 'registerResponse',
		component: () => import('@/views/register_response/index.vue'),
	},
	{
		path: '/:pathMatch(.*)*',
		name: 'notFound',
		redirect: '/404',
	},
]

export const router = createRouter({
	//  这里如果直接用webHistory模式，nginx部署时 xxx.86eb4fa7.js:1 Failed to load module script: Expected a JavaScript module script but the server responded with a MIME type of "text/html".
	history: createWebHashHistory(),
	routes,
	scrollBehavior: () => ({
		left: 0,
		top: 0
	}),
})

setupPageGuard(router)

export async function setupRouter(app: App) {
	app.use(router)
	await router.isReady()
}

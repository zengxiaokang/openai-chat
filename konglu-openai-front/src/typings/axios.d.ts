import {AxiosRequestConfig, AxiosResponse, AxiosInstance} from 'axios'

export interface ApiRequestInterceptors<T = AxiosResponse> {
	//定义扩展接口类型
	requestInterceptor?: (config: AxiosRequestConfig) => AxiosRequestConfig
	requestInterceptorCatch?: (error: any) => any
	// 响应拦截
	responseInterceptor?: <T = AxiosResponse>(config: T) => T
	responseInterceptorCatch?: (error: any) => any
}


export interface ApiRequestConfig<T = AxiosResponse>
	extends AxiosRequestConfig {
	//继承后有父类所有属性
	//涉及到接口的合并
	showLoading?: boolean
	interceptors?: ApiRequestInterceptors<T>
}

export interface ApiResponse<T = any> {
	data: T
	message: string | null
	code: number
}

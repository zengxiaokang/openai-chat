import { defHttp } from '@/utils/request'

export interface PicCodeResponse {
  base64Img: string
  uuid: string
}

export namespace captchaApi {
  /**
	 * 获取图片验证码
	 */
  export function getPicCode() {
    return defHttp.get<PicCodeResponse>({
      url: '/api/captcha/get_pic_code',
    })
  }

}

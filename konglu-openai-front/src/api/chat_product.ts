import {defHttp} from "@/utils/request";

export interface Top4ProductsResponseItem {
	id: string | number
	name: string
	price: number
	rewardUseTimes: number
	stock: number
}

export interface Top4ProductsResponse {
	products: Array<Top4ProductsResponseItem>
}

export namespace chatProductApi {

	/**
	 *  获取top4商品信息
	 */
	export function getTop4Products() {
		return defHttp.get<Top4ProductsResponse>({
			url: '/api/chat_product/get_top4_products',
		})
	}

}

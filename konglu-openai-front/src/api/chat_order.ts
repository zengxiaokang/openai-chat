import {PageRequest, PageResponse} from "@/api/base";
import {defHttp} from "@/utils/request";

export interface CreateOrderRequest {
	selectedProductItems: Array<SelectedProductItem>;
}

export interface SelectedProductItem {
	productId: number | string;
	num: number;
}

export interface TopProductItem {
	id: string | number
	name: string
	price: number
	rewardUseTimes: number
	stock: number,
	selectedCount: number
}

export interface MyOrderResponse {
	orderId: string;
	userId: string;
	payAmount: number;
	totalAmount: number;
	orderStatus: number;
	createTime: string;
	myOrderItems: Array<MyOrderItem>;
}

export interface MyOrderItem {
	orderId: string;
	orderItemId: string;
	productId: string;
	exchangeNum: number;
	productName: string;
	productPrice: number;
}

export interface MyOrdersRequest extends PageRequest {

}

export namespace chatOrderApi {
	// 创建订单
	export function createOrder(data: CreateOrderRequest) {
		return defHttp.post<string | number>({
			url: '/api/chat_order/create_order',
			data
		})
	}

	// 检查订单是否支付完成
	export function checkOrderStatusFinished(orderId: number | string) {
		return defHttp.get<boolean>({
			url: `/api/chat_order/check_order_status_finished/${orderId}`
		})
	}

	// 获取我的订单
	export function pageMyOrders(data: MyOrdersRequest) {
		return defHttp.post<PageResponse<MyOrderResponse>>({
			url: `/api/chat_order/page_my_orders`,
			data
		})
	}
	// 关闭我的订单
	export function closeMyOrder(orderId: number | string) {
		return defHttp.put<void>({
			url: `/api/chat_order/close_my_order/${orderId}`,
		})
	}
}

export interface PageResponse<T> {
	records: Array<T>
	pageNumber: number | string
	pageSize: number | string
	totalPage: number | string
	totalRow: number | string
}

export interface PageRequest {
	pageNum: number | string;
	pageSize: number | string;
}

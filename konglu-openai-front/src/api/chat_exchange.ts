import {PageRequest, PageResponse} from '@/api/base'
import {defHttp} from '@/utils/request'

export interface MyExchangeResponse {
	id: number;
	productId: number;
	name: string;
	code: string;
	rewardUseTimes: number;
	price: number;
	isUsed: boolean;
	expiresDays: number;
	activationTime: string;
}

export interface MyExchangesRequest extends PageRequest {

}

export namespace chatExchangeApi {

	/**
	 *  立即兑换聊天次数
	 */
	export function immediateExchangeConversation(code: string) {
		return defHttp.post<number>({
			url: `/api/chat_exchange/immediate_exchange_conversation?code=${code}`,
		})
	}

	// 分页查询我的兑换码
	export function pageMyExchanges(data: MyExchangesRequest) {
		return defHttp.post<PageResponse<MyExchangeResponse>>({
			url: `/api/chat_exchange/page_my_exchanges`,
			data
		})
	}
}

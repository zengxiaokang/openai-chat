import {defHttp} from "@/utils/request";

export interface UserInfoResponse {
	avatarUrl: string,
	nickname: string,
	description: string,
	userId: number | string,
	email: string,
	canConversationTimes: number | string
}

export interface LoginInfoResponse {
	token: string,
	userId: number | string,
}

/**
 * 前端用户登录请求
 */
export interface LoginByEmailRequest {
	// 密码
	password: string
	// 邮箱地址
	username: string
}

export enum RegisterTypeEnum {
	Email = 'email',
	Phone = 'phone',
}

export interface RegisterByEmailRequest {
	// 用户ID，可以为邮箱，可以为手机号
	identity: string
	// 密码信息，邮箱注册时需传入，手机注册时不用传入
	password: string
	//  图形验证码会话ID，必传
	uuid: string
	// 图片验证码，必传
	captcha: string
	// 注册类型
	registerType: RegisterTypeEnum
}

export namespace chatUserApi {
	/**
	 *  获取用户信息
	 */
	export function getUserInfo() {
		return defHttp.get<UserInfoResponse>({
			url: '/api/chat_user/get_user_info'
		})
	}

	/**
	 * 登录
	 */
	export function loginByEmail(data: LoginByEmailRequest) {
		return defHttp.post<LoginInfoResponse>({
			url: '/api/chat_user/login_by_email',
			data,
		})

	}

	/**
	 * 邮件验证回调验证码
	 */
	export function verifyEmailCode(code: string) {
		return defHttp.get<string>({
			url: '/api/chat_user/verify_email_code',
			data: {code},
		})
	}

	/**
	 * 注册
	 */
	export function registerByEmail(data: RegisterByEmailRequest) {
		return defHttp.post<string>({
			url: `/api/chat_user/register_by_email?uuid=${data.uuid}&code=${data.captcha}`,
			data: {
				identity: data.identity,
				password: data.password,
				registerType: data.registerType
			},
		})
	}

}



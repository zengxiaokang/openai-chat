import {defHttp} from "@/utils/request";

export interface AlipayFace2faceTradeRequest {
	orderNo: string | number;
}

export interface AlipayFace2faceTradeResponse {
	orderNo: string | number
	qrCode: string
	timeoutExpress: string
}

export namespace chatPayApi {
	// 面对面支付
	export function alipayFace2facePay(data: AlipayFace2faceTradeRequest) {
		return defHttp.post<AlipayFace2faceTradeResponse>({
			url: '/api/chat_pay/alipay/face2face_pay',
			data
		})
	}
}

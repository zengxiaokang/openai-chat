package validators

import (
	"github.com/go-playground/validator/v10"
	"regexp"
)

func ValidateMobile(fl validator.FieldLevel) bool {
	f := fl.Field().String()
	ok, _ := regexp.MatchString(`^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$`, f)
	return ok
}

func ValidateQQ(fl validator.FieldLevel) bool {
	f := fl.Field().String()
	ok, _ := regexp.MatchString(`^[1-9][0-9]{4,14}$`, f)
	return ok

}

func ValidateBigDecimalMoney(fl validator.FieldLevel) bool {
	f := fl.Field().String()
	ok, _ := regexp.MatchString(`^[1-9]+(.[0-9]{1,2})?$`, f)
	return ok
}

/*
*
当面付默认超时时间为 3h，timeout_express<=3h。
注意：当面付设置 timeout_express>3h 时，接口不报错，但是订单将在 3 小时关闭
*/
func ValidateFace2faceTimeoutExpress(fl validator.FieldLevel) bool {
	f := fl.Field().String()
	ok, _ := regexp.MatchString(`^[1-3]h$|^(1[0-8][0-9]|[1-9][0-9]|[1-9])m$`, f)
	return ok
}

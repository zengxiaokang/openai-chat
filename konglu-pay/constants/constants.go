package constants

const (
	TimeFormatFull           = "2006-01-02 15:04:05"
	TimeFormatYearMonthDay   = "2006-01-02"
	ChatOrderPurchaseSubject = "忻宇ai聊天使用次数购买"
)

const (
	BindingTypeEmail = "email"
	BindingTypePhone = "phone"
)

const (
	// 用户注册验证码认证
	RegisterVerify = 10
	// 用户找回密码验证码认证
	RetrievePassword = 11
	// 兑换码购买
	ChatExchangeCodePurchase = 12
)
const (
	StatusSendSuccess = 1
	StatusSendFailed  = 0

	DefaultSendSuccessMessage = "success"
)

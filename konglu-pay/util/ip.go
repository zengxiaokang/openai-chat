package util

import (
	"github.com/gin-gonic/gin"
	"strings"
)

func GetRequestIp(c *gin.Context) string {
	ip := c.Request.Header.Get("X-Forwarded-For")
	if ip == "" {
		ip = c.Request.Header.Get("X-Real-IP")
	}
	// 如果仍为空，则使用RemoteAddr属性获取IP地址
	if ip == "" {
		ip = strings.Split(c.Request.RemoteAddr, ":")[0]
	}
	return ip
}

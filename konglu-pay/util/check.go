package util

import (
	"errors"
	"gorm.io/gorm"
)

func GormRecordNotFound(err error) bool {
	return errors.Is(err, gorm.ErrRecordNotFound)
}

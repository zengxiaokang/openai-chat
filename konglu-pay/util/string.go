package util

import "strings"

// GetFirstUriPrefix 函数返回一个 URI 的前缀
func GetFirstUriPrefix(uri string) string {
	// 以 '/' 为分隔符，将 URI 拆分为多个子串
	parts := strings.Split(uri, "/")
	// 如果子串数量大于等于2，则第一个子串为 URI 前缀
	if len(parts) >= 2 {
		return "/" + parts[1]
	}
	return ""
}

// GetUriEndpointPrefix 函数返回 URI 的结尾部分
func GetUriEndpointPrefix(uri string) string {
	parts := strings.Split(uri, "/")
	return "/" + parts[len(parts)-1]
}

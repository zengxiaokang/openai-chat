package util

import (
	"konglu-pay/constants"
	"time"
)

func ParseTime(t time.Time) string {
	t, _ = time.Parse(time.RFC3339, t.String())
	ft := t.Format(constants.TimeFormatFull)
	return ft
}

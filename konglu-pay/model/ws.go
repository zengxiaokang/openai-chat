package model

type WsContent struct {
	Bytes []byte `json:"content"`
}

package model

type ChatOrderDO struct {
	BaseDO
	UserId      string   `json:"user_id"`
	TotalAmount float64  `json:"total_amount"`
	PayAmount   float64  `json:"pay_amount"`
	Status      int      `json:"status"`
	Type        int      `json:"type"`
	Remark      string   `json:"remark"`
	PayTime     JSONTime `json:"pay_time"`
	IsDeleted   int      `json:"is_deleted"`
}

func (s *ChatOrderDO) TableName() string {
	return "chat_order"
}

type ChatOrderItemDO struct {
	BaseDO
	OrderId      string  `json:"order_id"`
	ProductId    string  `json:"product_id"`
	ExchangeNum  int     `json:"exchange_num"`
	ProductName  string  `json:"product_name"`
	ProductPrice float64 `json:"product_price"`
	IsDeleted    int     `json:"is_deleted"`
}

func (s *ChatOrderItemDO) TableName() string {
	return "chat_order_item"
}

type ChatOrderHistoryDO struct {
	BaseDO
	UserId      string  `json:"user_id"`
	OrderId     string  `json:"order_id"`
	OrderStatus int     `json:"order_status"`
	PayStatus   int     `json:"pay_status"`
	TotalAmount float64 `json:"total_amount"`
	PayAmount   float64 `json:"pay_amount"`
	PayAccount  string  `json:"pay_account"`
	PayQrCode   string  `json:"pay_qr_code"`
	Remark      string  `json:"remark"`
	IsDeleted   int     `json:"is_deleted"`
}

func (s *ChatOrderHistoryDO) TableName() string {
	return "chat_order_history"
}

type ChatExchangeDO struct {
	BaseDO
	ProductId      string   `json:"product_id"`
	Name           string   `json:"name"`
	Code           string   `json:"code"`
	RewardUseTimes int      `json:"reward_use_times"`
	Price          float64  `json:"price"`
	IsUsed         int      `json:"is_used"`
	IsOccupied     int      `json:"is_occupied"`
	Status         int      `json:"status"`
	IsDeleted      int      `json:"isDeleted"`
	ExpiresDays    int      `json:"expiresDays"`
	ActivationTime JSONTime `json:"activation_time,omitempty"`
}

func (s *ChatExchangeDO) TableName() string {
	return "chat_exchange"
}

type ChatUserExchangeRecordDO struct {
	Id         string   `json:"id" gorm:"type:bigint(20);primary_key"`
	UserId     string   `json:"user_id"`
	ExchangeId string   `json:"exchange_id"`
	Code       string   `json:"code"`
	CreateTime JSONTime `json:"create_time,omitempty"`
}

func (s *ChatUserExchangeRecordDO) TableName() string {
	return "chat_user_exchange_record"
}

type ChatProductDO struct {
	BaseDO
	Name           string  `json:"name"`
	RewardUseTimes int     `json:"reward_use_times"`
	Price          float64 `json:"price"`
	Description    string  `json:"description"`
	Sale           int     `json:"sale"`
	Stock          int     `json:"stock"`
	Status         int     `json:"status"`
	IsDeleted      int     `json:"is_deleted"`
}

func (s *ChatProductDO) TableName() string {
	return "chat_product"
}

type ChatUserProductExchange struct {
	ProductName string   `json:"product_name"`
	ProductId   string   `json:"product_id"`
	UseTimes    int      `json:"use_times"`
	Price       float64  `json:"price"`
	Codes       []string `json:"codes"`
}

type ChatUserBindingDO struct {
	BaseDO
	BindingType        string `json:"binding_type"`
	UserBindingEmailId int    `json:"user_binding_email_id"`
	UserId             int    `json:"user_id"`
}

func (s *ChatUserBindingDO) TableName() string {
	return "chat_user_binding"
}

type ChatUserBindingEmailDO struct {
	BaseDO
	Username string `json:"username"`
	Password string `json:"password"`
	Verified bool   `json:"verified"` // gorm:"-"会不查询出来
}

func (s *ChatUserBindingEmailDO) TableName() string {
	return "chat_user_binding_email"
}

type ChatUserBindingEmailCombine struct {
	ChatUserBindingEmailDO
	Nickname string `json:"nickname"`
}

type SysEmailSendLogDO struct {
	BaseDO
	FromEmailAddress string `json:"from_email_address"`
	ToEmailAddress   string `json:"to_email_address"`
	BizType          int    `json:"biz_type"`
	RequestIp        string `json:"request_ip"`
	Content          string `json:"content"`
	Status           int    `json:"status"`
	MessageId        string `json:"message_id"`
	Message          string `json:"message"`
}

func (s *SysEmailSendLogDO) TableName() string {
	return "sys_email_send_log"
}

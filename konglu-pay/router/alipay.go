package router

import (
	"github.com/gin-gonic/gin"
	"konglu-pay/api"
)

func InitAlipayRouter(Router *gin.RouterGroup) {
	group := Router.Group("alipay")
	alipayApi := api.GroupApp.AlipayApi
	{
		group.POST("/face2face/pay", alipayApi.Face2facePay)
		group.POST("/face2face/notify", alipayApi.Face2faceNotify)
		//group.GET("/test", alipayApi.Test)
	}

}

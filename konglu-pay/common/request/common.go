package request

type PageInfo struct {
	Current int `json:"current" form:"current"`
	Size    int `json:"size" form:"size"`
}

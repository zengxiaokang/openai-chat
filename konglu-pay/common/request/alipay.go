package request

type EncodeAlipayTradePreCreateRequest struct {
	Base64 string `json:"base64" binding:"required"`
}

type AlipayTradePreCreateRequest struct {
	Subject        string `json:"subject" binding:"required,max=50,min=1"`
	TotalAmount    string `json:"total_amount" binding:"required,big_decimal_money"`
	OutTradeNo     string `json:"out_trade_no" binding:"required"`
	TimeoutExpress string `json:"timeout_express" binding:"required,face2face_timeout_express"`
	Signature      string `json:"signature" binding:"required"`
}

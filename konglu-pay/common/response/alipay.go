package response

type AlipayTradePreCreateResponse struct {
	OrderNo string `json:"order_no"`
	QrCode  string `json:"qr_code"`
}

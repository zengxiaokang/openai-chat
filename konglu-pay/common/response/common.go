package response

type PageResult struct {
	Records any   `json:"records"`
	Total   int64 `json:"total"`
	Current int64 `json:"current"`
	Size    int64 `json:"size"`
}

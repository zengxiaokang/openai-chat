package initialize

import (
	"github.com/smartwalle/alipay/v3"
	"konglu-pay/global"
)

func Alipay() {
	alipayConfig := global.Setting.AlipayConfig
	client, err := alipay.New(alipayConfig.Appid, alipayConfig.PrivateKey, true)
	if err != nil {
		panic(err)
	}
	err = client.LoadAliPayPublicKey(alipayConfig.PublicKey)
	if err != nil {
		panic(err)
	}
	global.AlipayClient = client
	global.Log.Info("alipay initialized ok")
}

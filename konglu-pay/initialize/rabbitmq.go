package initialize

import (
	"fmt"
	"github.com/streadway/amqp"
	"konglu-pay/global"
)

func RabbitMq() {
	conn, err := amqp.Dial(global.Setting.RabbitMqConfig.Url())
	if err != nil {
		panic(fmt.Sprintf("connection to rabbit failed: %v", err))
	}

	ch, err := conn.Channel()

	if err != nil {
		panic(fmt.Sprintf("open connection to rabbit failed: %v", err))
	}

	// 定义队列
	if !declareNoticeQueue(err, ch) {
		panic(fmt.Sprintf("declare rabbit mq failed: %v", err))
	}

	global.Amqp.Connection = conn
	global.Amqp.Channel = ch
	global.Log.Infof("open connection to rabbit ok")
}

func declareNoticeQueue(err error, ch *amqp.Channel) bool {

	return true
}

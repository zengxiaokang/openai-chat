package initialize

import (
	"github.com/gin-gonic/gin"
	"konglu-pay/global"
	"konglu-pay/middleware"
	"konglu-pay/router"
)

func Routers() *gin.Engine {

	Router := gin.New()
	//Router.LoadHTMLGlob("templates/*")
	//Router.GET("/", func(c *gin.Context) {
	//	c.HTML(http.StatusOK, "index.html", gin.H{
	//		"title": "Main website",
	//	})
	//})

	Router.NoMethod(middleware.RecoverNotFound)
	Router.NoRoute(middleware.RecoverNotFound)
	Router.Use(
		middleware.DefaultLogger(),
		middleware.TimeConsume(),
		middleware.Cors(),
		middleware.RecoverError,
	)
	globalGroup := Router.Group("/api")
	{
		router.InitAlipayRouter(globalGroup)
	}
	global.Log.Info("router register ok")
	return Router
}

package initialize

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"konglu-pay/global"
)

func Mongo() {
	clientOptions := options.Client().ApplyURI(global.Setting.MongoConfig.Url())
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		global.Log.Errorf("connect to mongo failed: %v", err)
		panic(err)
	}
	err = client.Ping(context.Background(), nil)
	if err != nil {
		global.Log.Errorf("ping to mongo failed: %v", err)
		panic(err)
	}
	global.MongoClient = client
	global.Log.Info("connect to mongo ok")

}

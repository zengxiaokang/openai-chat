package initialize

import (
	"gopkg.in/gomail.v2"
	"konglu-pay/global"
)

func Email() {
	emailConfig := global.Setting.EmailConfig
	dialer := gomail.NewDialer(emailConfig.Host, emailConfig.Port, emailConfig.User, emailConfig.Pass)
	global.EmailDialer = dialer
	global.Log.Infof("build email dialer ok")
}

package initialize

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"konglu-pay/core"
	"konglu-pay/global"
	"log"
	"os"
	"time"
)

func MysqlGorm() {
	m := global.Setting.MysqlConfig
	if m.DbName == "" {
		panic("database name cannot be empty")
	}
	mysqlConfig := mysql.Config{
		DSN:                       m.Dsn(), // DSN data source name
		DefaultStringSize:         191,     // string 类型字段的默认长度
		SkipInitializeWithVersion: false,   // 根据版本自动配置
	}
	if db, err := gorm.Open(mysql.New(mysqlConfig), config(m.Prefix, m.Singular)); err != nil {
		panic(fmt.Sprintf("mysql open error: %v", err))
	} else {
		db.InstanceSet("gorm:table_options", "ENGINE="+m.Engine)
		sqlDB, _ := db.DB()
		sqlDB.SetMaxIdleConns(m.MaxIdleConns)
		sqlDB.SetMaxOpenConns(m.MaxOpenConns)
		global.Dao = db
		global.Log.Info("open mysql gorm ok")
	}
}

// config gorm 自定义配置
func config(prefix string, singular bool) *gorm.Config {
	gormConfig := &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   prefix,
			SingularTable: singular,
		},
		DisableForeignKeyConstraintWhenMigrating: true,
	}
	_default := logger.New(core.NewWriter(log.New(os.Stdout, "\r\n", log.LstdFlags)), logger.Config{
		SlowThreshold: 200 * time.Millisecond,
		LogLevel:      logger.Warn,
		Colorful:      true,
	})
	var logMode = &global.Setting.MysqlConfig
	switch logMode.GetLogMode() {
	case "silent", "Silent":
		gormConfig.Logger = _default.LogMode(logger.Silent)
	case "error", "Error":
		gormConfig.Logger = _default.LogMode(logger.Error)
	case "warn", "Warn":
		gormConfig.Logger = _default.LogMode(logger.Warn)
	case "info", "Info":
		gormConfig.Logger = _default.LogMode(logger.Info)
	default:
		gormConfig.Logger = _default.LogMode(logger.Info)
	}
	return gormConfig
}

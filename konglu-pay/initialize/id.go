package initialize

import (
	"github.com/bwmarrin/snowflake"
	"konglu-pay/global"
)

func IdGenerator() {
	node, err := snowflake.NewNode(1)
	if err != nil {
		panic(err)
	}
	global.IdGenerator = node
	global.Log.Info("init id_generator ok")
}

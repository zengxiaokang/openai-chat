package initialize

import (
	"fmt"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"konglu-pay/constants"
	"konglu-pay/global"
	"konglu-pay/util"
	"os"
	"path"
	"time"
)

// getWriteSyncer 获取 zapcore.WriteSyncer
func getWriteSyncer(level string) (zapcore.WriteSyncer, error) {
	fileWriter, err := rotatelogs.New(
		path.Join(global.Setting.ZapConfig.Director, "%Y-%m-%d", level+".log"),
		rotatelogs.WithClock(rotatelogs.Local),
		rotatelogs.WithMaxAge(time.Duration(global.Setting.ZapConfig.MaxAge)*24*time.Hour), // 日志留存时间
		rotatelogs.WithRotationTime(time.Hour*24),
	)
	if global.Setting.ZapConfig.LogInConsole {
		return zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(fileWriter)), err
	}
	return zapcore.AddSync(fileWriter), err
}

// getEncoder 获取 zapcore.Encoder
func getEncoder() zapcore.Encoder {
	if global.Setting.ZapConfig.Format == "json" {
		return zapcore.NewJSONEncoder(getEncoderConfig())
	}
	return zapcore.NewConsoleEncoder(getEncoderConfig())
}

// getEncoderConfig 获取zapcore.EncoderConfig
func getEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		MessageKey:     "message",
		LevelKey:       "level",
		TimeKey:        "time",
		NameKey:        "logger",
		CallerKey:      "caller",
		StacktraceKey:  global.Setting.ZapConfig.StacktraceKey,
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    global.Setting.ZapConfig.ZapEncodeLevel(),
		EncodeTime:     customTimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.FullCallerEncoder,
	}
}

// getEncoderCore 获取Encoder的 zapcore.Core
func getEncoderCore(l zapcore.Level, level zap.LevelEnablerFunc) zapcore.Core {
	// 使用file-rotatelogs进行日志分割
	writer, err := getWriteSyncer(l.String())
	if err != nil {
		zap.S().Infof("get write syncer failed err: %v", err)
		return nil
	}
	return zapcore.NewCore(getEncoder(), writer, level)
}

// customTimeEncoder 自定义日志输出时间格式
func customTimeEncoder(t time.Time, encoder zapcore.PrimitiveArrayEncoder) {
	encoder.AppendString(global.Setting.ZapConfig.Prefix + t.Format(constants.TimeFormatFull))
}

// getZapCores 根据配置文件的Level获取 []zapcore.Core
func getZapCores() []zapcore.Core {
	cores := make([]zapcore.Core, 0, 7)
	for level := global.Setting.ZapConfig.TransportLevel(); level <= zapcore.FatalLevel; level++ {
		cores = append(cores, getEncoderCore(level, getLevelPriority(level)))
	}
	return cores
}

// getLevelPriority 根据 zapcore.Level 获取 zap.LevelEnablerFunc
func getLevelPriority(level zapcore.Level) zap.LevelEnablerFunc {
	switch level {
	case zapcore.DebugLevel:
		return func(level zapcore.Level) bool { // 调试级别
			return level == zap.DebugLevel
		}
	case zapcore.InfoLevel:
		return func(level zapcore.Level) bool { // 日志级别
			return level == zap.InfoLevel
		}
	case zapcore.WarnLevel:
		return func(level zapcore.Level) bool { // 警告级别
			return level == zap.WarnLevel
		}
	case zapcore.ErrorLevel:
		return func(level zapcore.Level) bool { // 错误级别
			return level == zap.ErrorLevel
		}
	case zapcore.DPanicLevel:
		return func(level zapcore.Level) bool { // dpanic级别
			return level == zap.DPanicLevel
		}
	case zapcore.PanicLevel:
		return func(level zapcore.Level) bool { // panic级别
			return level == zap.PanicLevel
		}
	case zapcore.FatalLevel:
		return func(level zapcore.Level) bool { // 终止级别
			return level == zap.FatalLevel
		}
	default:
		return func(level zapcore.Level) bool { // 调试级别
			return level == zap.DebugLevel
		}
	}
}

// ZapLogger  获取 zap.Logger
func ZapLogger() {
	// 判断是否有Director文件夹
	if ok, _ := util.PathExists(global.Setting.ZapConfig.Director); !ok {
		fmt.Printf("create %v directory\n", global.Setting.ZapConfig.Director)
		_ = os.Mkdir(global.Setting.ZapConfig.Director, os.ModePerm)
	}
	cores := getZapCores()
	logger := zap.New(zapcore.NewTee(cores...))
	if global.Setting.ZapConfig.ShowLine {
		logger = logger.WithOptions(zap.AddCaller())
	}
	global.Log = logger.Sugar()
	zap.ReplaceGlobals(logger)
	global.Log.Info("init zap logger ok")
}

package config

type (
	RedisConfig struct {
		Database int                `mapstructure:"database" json:"database" yaml:"database" default:"0"`
		Host     string             `mapstructure:"host" json:"host" yaml:"host" default:"127.0.0.1"`
		Port     int                `mapstructure:"port" json:"port" yaml:"port" default:"6379"`
		Username string             `mapstructure:"username" json:"username" yaml:"username"`
		Password string             `mapstructure:"password" json:"password" yaml:"password"`
		Ping     bool               `mapstructure:"ping" json:"ping" yaml:"ping" default:"true"`
		Cluster  RedisClusterConfig `mapstructure:"cluster" json:"cluster" yaml:"cluster" `
	}
	RedisClusterConfig struct {
		Nodes []string `mapstructure:"nodes" json:"nodes" yaml:"nodes" `
	}
)

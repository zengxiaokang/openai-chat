package config

import "fmt"

type MongoConfig struct {
	Host     string `mapstructure:"host" json:"host" yaml:"host" default:"127.0.0.1"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port" default:"27017"`
	Database string `mapstructure:"database" json:"database" yaml:"database" default:"admin"`
	Username string `mapstructure:"username" json:"username" yaml:"username" default:"admin"`
	Password string `mapstructure:"password" json:"password" yaml:"password"`
	Config   string `mapstructure:"config" json:"config" yaml:"config"`
}

func (r *MongoConfig) Url() string {
	if len(r.Config) != 0 {
		return fmt.Sprintf("mongodb://%s:%s@%s:%d/%s?%s", r.Username, r.Password, r.Host, r.Port, r.Database, r.Config)
	}
	return fmt.Sprintf("mongodb://%s:%s@%s:%d/%s", r.Username, r.Password, r.Host, r.Port, r.Database)
}
func (r *MongoConfig) ApplyURI() string {
	return fmt.Sprintf("mongodb://%s:%d", r.Host, r.Port)
}

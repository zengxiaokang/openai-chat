package config

type RsaRequestConfig struct {
	PrivateKey string `mapstructure:"private_key"  json:"private_key"`
	PublicKey  string `mapstructure:"public_key"  json:"public_key"`
}

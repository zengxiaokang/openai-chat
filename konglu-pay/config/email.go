package config

type EmailConfig struct {
	From string `mapstructure:"from" json:"from" yaml:"from"`
	Host string `mapstructure:"host" json:"host" yaml:"host"`
	Port int    `mapstructure:"port" json:"port" yaml:"port"`
	User string `mapstructure:"user" json:"user" yaml:"user"`
	Pass string `mapstructure:"pass" json:"pass" yaml:"pass"`
}

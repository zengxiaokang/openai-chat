package config

type AlipayConfig struct {
	Appid      string `mapstructure:"appid" json:"appid" yaml:"appid"`
	PrivateKey string `mapstructure:"private_key" json:"private_key" yaml:"private_key"`
	PublicKey  string `mapstructure:"public_key" json:"public_key" yaml:"public_key"`
	NotifyUrl  string `mapstructure:"notify_url" json:"notify_url" yaml:"notify_url"`
	ServerUrl  string `mapstructure:"server_url" json:"server_url" yaml:"server_url"`
	Signature  string `mapstructure:"signature" json:"signature" yaml:"signature"`
}

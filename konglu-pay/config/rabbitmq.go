package config

import (
	"fmt"
)

type RabbitMqConfig struct {
	Host     string `mapstructure:"host" json:"host" yaml:"host" default:"127.0.0.1"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port" default:"15672"`
	Username string `mapstructure:"username" json:"username" yaml:"username" default:"guest"`
	Password string `mapstructure:"password" json:"password" yaml:"password" default:"guest"`
	VHost    string `mapstructure:"vhost" json:"vhost" yaml:"vhost"`
}

func (r *RabbitMqConfig) Url() string {
	return fmt.Sprintf("amqp://%s:%s@%s:%d%s", r.Username, r.Password, r.Host, r.Port, r.VHost)
}

package config

type JwtConfig struct {
	Token      string `mapstructure:"token" json:"token" yaml:"token"`
	SigningKey string `mapstructure:"secret" json:"secret" yaml:"secret"`
	Expire     int64  `mapstructure:"expire" json:"expire" yaml:"expire"`
	Issuer     string `mapstructure:"issuer" json:"issuer" yaml:"issuer"`
}

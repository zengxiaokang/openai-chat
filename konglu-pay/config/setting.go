package config

type Setting struct {
	ServerConfig     `mapstructure:"server" json:"server" yaml:"server"`
	JwtConfig        `mapstructure:"jwt" json:"jwt" yaml:"jwt"`
	ZapConfig        `mapstructure:"zap" json:"zap" yaml:"zap"`
	MysqlConfig      `mapstructure:"mysql" json:"mysql" yaml:"mysql"`
	RedisConfig      `mapstructure:"redis" json:"redis" yaml:"redis"`
	MinioConfig      `mapstructure:"minio" json:"minio" yaml:"minio"`
	RabbitMqConfig   `mapstructure:"rabbitmq" json:"rabbitmq" yaml:"rabbitmq"`
	MongoConfig      `mapstructure:"mongo" json:"mongo" yaml:"mongo"`
	AlipayConfig     `mapstructure:"alipay" json:"alipay" yaml:"alipay"`
	RsaRequestConfig `mapstructure:"rsa_request" json:"rsa_request" yaml:"rsa_request"`
	EmailConfig      `mapstructure:"email" json:"email" yaml:"email"`
}

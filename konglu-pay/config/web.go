package config

type ServerConfig struct {
	Port   uint   `mapstructure:"port" json:"port" yaml:"port" default:"8080"` // 端口值
	Locale string `mapstructure:"locale" json:"locale" yaml:"locale" default:"en"`
}

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"html/template"
	"konglu-pay/constants"
	"konglu-pay/global"
	"konglu-pay/initialize"
	"os"
	"testing"
	"time"
)

type ChatUserExchangeRecordDO struct {
	Code string
}
type ChatUserProductExchange struct {
	ProductName string   `json:"product_name"`
	ProductId   string   `json:"product_id"`
	UseTimes    int      `json:"use_times"`
	Price       float64  `json:"price"`
	Codes       []string `json:"codes"`
}

func TestTemplate(t *testing.T) {
	initialize.Viper()
	var body bytes.Buffer
	tmpl, err := template.ParseFiles("./templates/exchange_code.html")
	if err != nil {
		fmt.Println(err)
		return
	}
	userExchanges := make([]ChatUserProductExchange, 0)
	userExchanges = append(userExchanges, ChatUserProductExchange{
		ProductName: "兑换100次聊天",
		ProductId:   "",
		UseTimes:    100,
		Price:       0.1,
		Codes:       []string{"YKVKZSWFR2UKU4PEWTYA", "YKVKZSWFR2UKU4PEWTYb"},
	})
	userExchanges = append(userExchanges, ChatUserProductExchange{
		ProductName: "兑换200次聊天",
		ProductId:   "",
		UseTimes:    200,
		Price:       0.2,
		Codes:       []string{"YKVKZSWFR2UKU4PEWTCA", "YKVKZSWFR2UKU4PEWTYD", "YKVKZSWFR2UKU4PEWTYD", "YKVKZSWFR2UKU4PEWTYD"},
	})

	err = tmpl.Execute(&body, map[string]any{
		"UserExchanges": userExchanges,
		"TotalAmount":   100,
		"OrderNo":       "11531313131313513513",
		"PayTime":       time.Now().Format(constants.TimeFormatFull),
		"ExpireTime":    time.Now().AddDate(0, 0, 90).Format(constants.TimeFormatFull),
		"NickName":      "linc",
	})
	if err != nil {
		global.Log.Errorf("执行模板异常: %v", err)
		return
	}
	emailContent := body.String()
	file, err := os.Create("./templates/exchange_code_preview.html")
	if err != nil {
		panic(err)
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)
	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(emailContent)
	if err != nil {
		return
	}

}

type ChatUserBindingEmailDO struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Verified bool   `json:"verified"`
	Nickname string `json:"nickname"`
}

func TestQuery(t *testing.T) {
	initialize.Viper()
	initialize.ZapLogger()
	initialize.MysqlGorm()
	userBindingEmail := new(ChatUserBindingEmailDO)
	err := global.Dao.Table("chat_user_binding_email cubel").
		Select("cubel.username,cu.nickname").
		Joins("left join chat_user_binding cub ON cubel.id = cub.user_binding_email_id ").
		Joins("inner join chat_user cu on cub.user_id = cu.id").
		Where("cub.binding_type = ? and cub.user_id = ? and cubel.verified = 1", constants.BindingTypeEmail, "1655238682885054465").
		First(userBindingEmail).Error
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(userBindingEmail)
}

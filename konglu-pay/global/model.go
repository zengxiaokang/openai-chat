package global

import (
	"github.com/go-redis/redis/v8"
	"github.com/go-redsync/redsync/v4"
	"github.com/streadway/amqp"
	"golang.org/x/crypto/bcrypt"
)

type RedisClientManager struct {
	Client        *redis.Client
	ClusterClient *redis.ClusterClient
	Mutex         *redsync.Redsync
}

// BcryptPasswordEncoder 是一个使用bcrypt算法的密码编码器
type BcryptPasswordEncoder struct {
	cost int // bcrypt算法的加密成本，数值越大，加密越慢，但越安全
}

// NewBcryptPasswordEncoder 创建一个新的bcryptPasswordEncoder实例
func NewBcryptPasswordEncoder(cost int) *BcryptPasswordEncoder {
	return &BcryptPasswordEncoder{
		cost: cost,
	}
}

// Encode 使用bcrypt算法对密码进行编码
func (p *BcryptPasswordEncoder) Encode(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), p.cost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// Matches 检查密码是否与编码后的密码匹配
func (p *BcryptPasswordEncoder) Matches(password, encodedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(encodedPassword), []byte(password))
	return err == nil
}

type RabbitMq struct {
	*amqp.Connection
	*amqp.Channel
}

func NewRabbitMq() *RabbitMq {
	return &RabbitMq{}
}

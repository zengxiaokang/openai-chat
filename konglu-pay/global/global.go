package global

import (
	"github.com/bwmarrin/snowflake"
	ut "github.com/go-playground/universal-translator"
	"github.com/gorilla/websocket"
	"github.com/patrickmn/go-cache"
	"github.com/sashabaranov/go-openai"
	"github.com/smartwalle/alipay/v3"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	gomail "gopkg.in/gomail.v2"
	"gorm.io/gorm"
	"konglu-pay/config"
	"konglu-pay/timer"
	"net/http"
	"time"
)

var (
	Dao                *gorm.DB
	Log                *zap.SugaredLogger
	Setting            = &config.Setting{}
	RCManager          = &RedisClientManager{}
	ValidateTranslator ut.Translator
	IdGenerator        = &snowflake.Node{}
	PasswordEncoder    = NewBcryptPasswordEncoder(12)
	LocalCache         = cache.New(5*time.Minute, 10*time.Minute)
	Amqp               = NewRabbitMq()
	UpGrader           = websocket.Upgrader{
		ReadBufferSize:   1024,
		WriteBufferSize:  1024,
		HandshakeTimeout: 5 * time.Second,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	OpenaiClient = &openai.Client{}
	MongoClient  = &mongo.Client{}
	Timer        = timer.NewTimerTask()
	AlipayClient = &alipay.Client{}
	EmailDialer  = &gomail.Dialer{}
)

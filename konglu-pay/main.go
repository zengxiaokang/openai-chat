package main

import (
	"fmt"
	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"konglu-pay/global"
	"konglu-pay/initialize"
	"time"
)

type App struct {
}

func main() {
	app := new(App)
	initialize.Viper()
	initialize.ZapLogger()
	initialize.IdGenerator()
	initialize.Translator()
	initialize.Redis()
	initialize.MysqlGorm()
	initialize.Email()
	initialize.Alipay()
	app.initRouters()
}
func (app *App) initRouters() {
	router := initialize.Routers()
	app.startApp(router)
}

func (*App) startApp(router *gin.Engine) {
	port := fmt.Sprintf(":%d", global.Setting.ServerConfig.Port)
	s := endless.NewServer(port, router)
	s.ReadHeaderTimeout = 20 * time.Second
	s.WriteTimeout = 20 * time.Second
	s.MaxHeaderBytes = 1 << 20
	time.Sleep(10 * time.Microsecond)
	global.Log.Info("server run success on ", zap.String("port", port))
	global.Log.Error(s.ListenAndServe().Error())
}

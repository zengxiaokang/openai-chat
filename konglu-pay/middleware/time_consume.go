package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"konglu-pay/global"
	"time"
)

func TimeConsume() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 记录开始时间
		start := time.Now()
		// 继续执行后续处理函数
		c.Next()
		// 记录结束时间
		end := time.Now()
		// 计算接口耗时
		latency := end.Sub(start)
		// 将接口耗时添加到响应头中
		c.Writer.Header().Add("X-Api-Latency", fmt.Sprintf("%v", latency))
		global.Log.Infof(fmt.Sprintf("request path: %s consume: %d ", c.FullPath(), latency))
	}
}

# 忻宇ai

**数据库名：** open_ai_chat

**文档版本：** 1.0

**文档描述：** 数据库文档

| 表名                  | 说明       |
| :---: | :---: |
| [chat_exchange](#chat_exchange) | Chat聊天兑换表 |
| [chat_message](#chat_message) | Chat聊天记录表 |
| [chat_order](#chat_order) | Chat订单信息表 |
| [chat_order_delay_failure_record](#chat_order_delay_failure_record) | Chat超时订单失败记录表 |
| [chat_order_history](#chat_order_history) | Chat订单历史记录表 |
| [chat_order_item](#chat_order_item) | Chat订单明细表 |
| [chat_product](#chat_product) | Chat商品表 |
| [chat_room](#chat_room) | chat聊天室表 |
| [chat_sensitive_word](#chat_sensitive_word) | Chat敏感词表 |
| [chat_user](#chat_user) | Chat用户表 |
| [chat_user_binding](#chat_user_binding) | Chat用户绑定关系表 |
| [chat_user_binding_email](#chat_user_binding_email) | Chat用户邮箱绑定表 |
| [chat_user_exchange_record](#chat_user_exchange_record) | Chat用户聊天兑换记录表 |
| [chat_user_login_log](#chat_user_login_log) | Chat用户登录日志表 |
| [sys_email_send_log](#sys_email_send_log) | Sys系统邮箱发送日志表 |
| [sys_email_verify_code](#sys_email_verify_code) | Sys系统邮箱验证码表 |

**表名：** <a id="chat_exchange">chat_exchange</a>

**说明：** Chat聊天兑换表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | product_id |   bigint   | 20 |   0    |    N     |  N   |       | 商品id#chat_product  |
|  3   | name |   varchar   | 30 |   0    |    N     |  N   |       | 名称  |
|  4   | code |   varchar   | 100 |   0    |    N     |  N   |       | 通兑码  |
|  5   | reward_use_times |   int   | 10 |   0    |    N     |  N   |   0    | 奖励次数  |
|  6   | price |   decimal   | 11 |   2    |    N     |  N   |   0.00    | 面值  |
|  7   | is_used |   bit   | 1 |   0    |    N     |  N   |   0    | 是否使用，false否，true是  |
|  8   | is_occupied |   bit   | 1 |   0    |    N     |  N   |   0    | 是否被占用:0否1是  |
|  9   | status |   bit   | 1 |   0    |    N     |  N   |   0    | 状态1:启用2:停用  |
|  10   | is_deleted |   int   | 10 |   0    |    N     |  N   |   0    | 是否删除0否1是  |
|  11   | activation_time |   datetime   | 19 |   0    |    Y     |  N   |       | 激活时间  |
|  12   | expires_days |   int   | 10 |   0    |    Y     |  N   |   0    | 多少天后过期  |
|  13   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  14   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_message">chat_message</a>

**说明：** Chat聊天记录表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 用户id#chat_user  |
|  3   | message_id |   varchar   | 255 |   0    |    N     |  N   |       | 消息id  |
|  4   | parent_message_id |   varchar   | 255 |   0    |    Y     |  N   |       | 父级消息id  |
|  5   | parent_answer_message_id |   varchar   | 255 |   0    |    Y     |  N   |       | 父级回答消息id  |
|  6   | parent_question_message_id |   varchar   | 255 |   0    |    Y     |  N   |       | 父级问题消息id  |
|  7   | context_count |   bigint   | 20 |   0    |    N     |  N   |       | 上下文数量  |
|  8   | question_context_count |   bigint   | 20 |   0    |    N     |  N   |       | 问题上下文数量  |
|  9   | message_type |   int   | 10 |   0    |    N     |  N   |       | 消息类型枚举  |
|  10   | chat_room_id |   bigint   | 20 |   0    |    N     |  N   |       | 聊天室id#chat_room  |
|  11   | conversation_id |   varchar   | 255 |   0    |    Y     |  N   |       | 对话id  |
|  12   | api_type |   varchar   | 20 |   0    |    N     |  N   |       | API类型  |
|  13   | model_name |   varchar   | 50 |   0    |    Y     |  N   |       | 模型名称  |
|  14   | api_key |   varchar   | 255 |   0    |    Y     |  N   |       | ApiKey  |
|  15   | content |   varchar   | 5000 |   0    |    N     |  N   |       | 消息内容  |
|  16   | original_data |   text   | 65535 |   0    |    Y     |  N   |       | 消息的原始数据  |
|  17   | response_error_data |   text   | 65535 |   0    |    Y     |  N   |       | 错误响应数据  |
|  18   | prompt_tokens |   bigint   | 20 |   0    |    Y     |  N   |       | 输入消息的tokens  |
|  19   | completion_tokens |   bigint   | 20 |   0    |    Y     |  N   |       | 输出消息的tokens  |
|  20   | total_tokens |   bigint   | 20 |   0    |    Y     |  N   |       | 累计Tokens  |
|  21   | ip |   varchar   | 255 |   0    |    Y     |  N   |       | ip  |
|  22   | status |   int   | 10 |   0    |    N     |  N   |       | 聊天信息状态  |
|  23   | is_hide |   bit   | 1 |   0    |    N     |  N   |   0    | 是否被隐藏  |
|  24   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  25   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_order">chat_order</a>

**说明：** Chat订单信息表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 用户id#chat_user  |
|  3   | total_amount |   decimal   | 11 |   2    |    N     |  N   |       | 订单总金额  |
|  4   | pay_amount |   decimal   | 11 |   2    |    N     |  N   |       | 应付金额  |
|  5   | status |   tinyint   | 4 |   0    |    N     |  N   |   0    | 订单状态(0:待付款1:待确认2:已完成3:无效订单4已关闭  |
|  6   | type |   tinyint   | 4 |   0    |    N     |  N   |   0    | 订单类型(0:正常订单1:秒杀订单)  |
|  7   | remark |   varchar   | 500 |   0    |    Y     |  N   |       | 备注  |
|  8   | pay_time |   datetime   | 19 |   0    |    Y     |  N   |       | 支付时间  |
|  9   | is_deleted |   int   | 10 |   0    |    N     |  N   |   0    | 是否删除0否1是  |
|  10   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  11   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_order_delay_failure_record">chat_order_delay_failure_record</a>

**说明：** Chat超时订单失败记录表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 主键  |
|  2   | error_message |   text   | 65535 |   0    |    N     |  N   |       | 错误信息  |
|  3   | message_payload |   longtext   | 2147483647 |   0    |    N     |  N   |       | 消息内容json  |
|  4   | retry_count |   int   | 10 |   0    |    N     |  N   |   0    | 重试次数  |
|  5   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |

**表名：** <a id="chat_order_history">chat_order_history</a>

**说明：** Chat订单历史记录表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 用户id#chat_user  |
|  3   | order_id |   bigint   | 20 |   0    |    N     |  N   |       | 订单id#chat_order  |
|  4   | order_status |   tinyint   | 4 |   0    |    N     |  N   |   0    | 订单状态0:待付款1:待确认2:已完成3:无效订单4:已关闭#chat_order  |
|  5   | pay_status |   tinyint   | 4 |   0    |    N     |  N   |   0    | 支付状态0:支付中(0:待付款1:待确认)1:支付成功(2:已完成)2:支付失败(3:无效订单)3:支付关闭  |
|  6   | total_amount |   decimal   | 11 |   2    |    N     |  N   |   0.00    | 订单总金额  |
|  7   | pay_amount |   decimal   | 11 |   2    |    N     |  N   |       | 应付金额  |
|  8   | pay_account |   varchar   | 30 |   0    |    Y     |  N   |       | 支付账户  |
|  9   | pay_qr_code |   varchar   | 255 |   0    |    Y     |  N   |       | 支付二维码链接  |
|  10   | is_deleted |   int   | 10 |   0    |    N     |  N   |   0    | 是否删除0否1是  |
|  11   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  12   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |
|  13   | remark |   varchar   | 500 |   0    |    Y     |  N   |       | 备注  |

**表名：** <a id="chat_order_item">chat_order_item</a>

**说明：** Chat订单明细表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | order_id |   bigint   | 20 |   0    |    N     |  N   |       | 订单id#chat_order  |
|  3   | product_id |   bigint   | 20 |   0    |    N     |  N   |       | 商品id#chat_product  |
|  4   | exchange_num |   int   | 10 |   0    |    N     |  N   |       | 兑换码数量  |
|  5   | product_name |   varchar   | 200 |   0    |    N     |  N   |       | 商品名称#chat_product  |
|  6   | product_price |   decimal   | 11 |   2    |    N     |  N   |       | 商品价格#chat_product  |
|  7   | is_deleted |   int   | 10 |   0    |    N     |  N   |   0    | 是否删除0否1是  |
|  8   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  9   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_product">chat_product</a>

**说明：** Chat商品表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | name |   varchar   | 30 |   0    |    N     |  N   |       | 名称  |
|  3   | reward_use_times |   int   | 10 |   0    |    N     |  N   |   0    | 奖励次数  |
|  4   | price |   decimal   | 11 |   2    |    N     |  N   |   0.00    | 价格  |
|  5   | description |   varchar   | 64 |   0    |    N     |  N   |       | 描述  |
|  6   | sale |   int   | 10 |   0    |    N     |  N   |   0    | 销量  |
|  7   | stock |   int   | 10 |   0    |    N     |  N   |   0    | 库存  |
|  8   | status |   bit   | 1 |   0    |    N     |  N   |   0    | 状态1:启用2:停用  |
|  9   | is_deleted |   int   | 10 |   0    |    N     |  N   |   0    | 是否删除0否1是  |
|  10   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  11   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_room">chat_room</a>

**说明：** chat聊天室表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 用户id#chat_user  |
|  3   | ip |   varchar   | 255 |   0    |    Y     |  N   |       | ip  |
|  4   | conversation_id |   varchar   | 255 |   0    |    Y     |  N   |       | 对话id  |
|  5   | first_chat_message_id |   bigint   | 20 |   0    |    N     |  N   |       | 第一条消息主键  |
|  6   | first_message_id |   varchar   | 255 |   0    |    N     |  N   |       | 第一条消息id  |
|  7   | title |   varchar   | 5000 |   0    |    N     |  N   |       | 对话标题  |
|  8   | api_type |   varchar   | 20 |   0    |    N     |  N   |       | API类型  |
|  9   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  10   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_sensitive_word">chat_sensitive_word</a>

**说明：** Chat敏感词表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | word |   varchar   | 255 |   0    |    N     |  N   |       | 敏感词  |
|  3   | status |   int   | 10 |   0    |    N     |  N   |       | 状态1启用2停用  |
|  4   | is_deleted |   int   | 10 |   0    |    Y     |  N   |   0    | 是否删除0否1是  |
|  5   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  6   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_user">chat_user</a>

**说明：** Chat用户表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | 用户ID  |
|  2   | nickname |   varchar   | 16 |   0    |    N     |  N   |       | 用户昵称  |
|  3   | description |   varchar   | 64 |   0    |    Y     |  N   |       | 描述  |
|  4   | avatar_version |   int   | 10 |   0    |    N     |  N   |       | 头像版本  |
|  5   | last_login_ip |   varchar   | 64 |   0    |    Y     |  N   |       | 上一次登录IP  |
|  6   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  7   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |
|  8   | can_conversation_times |   int   | 10 |   0    |    Y     |  N   |   0    | 可聊天次数  |

**表名：** <a id="chat_user_binding">chat_user_binding</a>

**说明：** Chat用户绑定关系表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | binding_type |   varchar   | 16 |   0    |    N     |  N   |       | 绑定类型  |
|  3   | user_binding_email_id |   bigint   | 20 |   0    |    N     |  N   |       | 额外信息表的用户ID#chat_user_binding_email  |
|  4   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 基础用户表的ID#chat_user  |
|  5   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  6   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_user_binding_email">chat_user_binding_email</a>

**说明：** Chat用户邮箱绑定表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | username |   varchar   | 64 |   0    |    N     |  N   |       | 用户名  |
|  3   | password |   varchar   | 128 |   0    |    N     |  N   |       | 密码  |
|  4   | verified |   tinyint   | 4 |   0    |    N     |  N   |   0    | 是否验证过，false否true是  |
|  5   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  6   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="chat_user_exchange_record">chat_user_exchange_record</a>

**说明：** Chat用户聊天兑换记录表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 用户id#chat_user  |
|  3   | exchange_id |   bigint   | 20 |   0    |    N     |  N   |       | 兑换id#chat_exchange  |
|  4   | code |   varchar   | 100 |   0    |    N     |  N   |       | 通兑码#chat_exchange  |
|  5   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |

**表名：** <a id="chat_user_login_log">chat_user_login_log</a>

**说明：** Chat用户登录日志表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | user_id |   bigint   | 20 |   0    |    N     |  N   |       | 登录的基础用户ID#chat_user  |
|  3   | login_type |   varchar   | 32 |   0    |    N     |  N   |       | 登录方式（注册方式），邮箱登录，手机登录等等  |
|  4   | user_binding_email_id |   bigint   | 20 |   0    |    N     |  N   |       | 登录信息ID与login_type有关联，邮箱登录时关联#chat_user_binding_email  |
|  5   | login_ip |   varchar   | 32 |   0    |    N     |  N   |       | 登录的IP地址  |
|  6   | login_status |   bit   | 1 |   0    |    N     |  N   |       | 登录状态，1登录成功，0登录失败  |
|  7   | message |   varchar   | 64 |   0    |    N     |  N   |       | 登录后返回的消息  |
|  8   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |

**表名：** <a id="sys_email_send_log">sys_email_send_log</a>

**说明：** Sys系统邮箱发送日志表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | from_email_address |   varchar   | 64 |   0    |    N     |  N   |       | 发件人邮箱  |
|  3   | to_email_address |   varchar   | 64 |   0    |    N     |  N   |       | 收件人邮箱  |
|  4   | biz_type |   int   | 10 |   0    |    N     |  N   |       | 业务类型(10:用户注册验证码认证11:用户找回密码验证码认证12:兑换码购买)  |
|  5   | request_ip |   varchar   | 255 |   0    |    N     |  N   |       | 请求ip  |
|  6   | content |   text   | 65535 |   0    |    N     |  N   |       | 发送内容  |
|  7   | message_id |   varchar   | 128 |   0    |    N     |  N   |       | 消息id  |
|  8   | status |   tinyint   | 4 |   0    |    N     |  N   |       | 发送状态，1成功，0失败  |
|  9   | message |   varchar   | 1024 |   0    |    N     |  N   |       | 发送后的消息，用于记录成功/失败的信息，成功默认为success  |
|  10   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  11   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

**表名：** <a id="sys_email_verify_code">sys_email_verify_code</a>

**说明：** Sys系统邮箱验证码表

**数据列：**

| 序号 | 名称 | 数据类型 |  长度  | 小数位 | 允许空值 | 主键 | 默认值 | 说明 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  1   | id |   bigint   | 20 |   0    |    N     |  Y   |       | ID  |
|  2   | to_email_address |   varchar   | 64 |   0    |    N     |  N   |       | 验证码接收邮箱地址  |
|  3   | verify_code |   varchar   | 32 |   0    |    N     |  N   |       | 验证码唯一  |
|  4   | is_used |   bit   | 1 |   0    |    N     |  Y   |       | 是否使用，false否，true是  |
|  5   | verify_ip |   varchar   | 100 |   0    |    N     |  N   |       | 核销IP，方便识别一些机器人账号  |
|  6   | expire_at |   datetime   | 19 |   0    |    N     |  N   |       | 验证码过期时间  |
|  7   | biz_type |   tinyint   | 4 |   0    |    N     |  N   |       | 当前邮箱业务  |
|  8   | create_time |   datetime   | 19 |   0    |    N     |  N   |       | 创建时间  |
|  9   | update_time |   datetime   | 19 |   0    |    N     |  N   |       | 更新时间  |

package linc.fun.openai.util;

import cn.hutool.core.lang.generator.SnowflakeGenerator;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;


@Component
public class IdGenerator {

    @Resource
    private SnowflakeGenerator snowflakeGenerator;

    /**
     * 获取uid
     */
    public long getId() {
        return snowflakeGenerator.next();
    }

    public String getIdStr() {
        return snowflakeGenerator.next().toString();
    }

}

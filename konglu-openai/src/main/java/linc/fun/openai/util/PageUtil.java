package linc.fun.openai.util;

import cn.hutool.core.bean.BeanUtil;
import com.mybatisflex.core.paginate.Page;
import lombok.experimental.UtilityClass;

import java.util.List;

/**
 * @author linc
 * @date 2023-3-27
 * 分页工具
 */
@UtilityClass
public class PageUtil {

    /**
     * IPage 转 Page
     *
     * @param page   IPage
     * @param target 需要 copy 转换的类型
     * @param <T>    泛型
     * @return PageResult
     */
    public static <T> Page<T> toPage(Page<?> page, Class<T> target) {
        return toPage(page, BeanUtil.copyToList(page.getRecords(), target));
    }

    /**
     * IPage 转 Page
     *
     * @param page    IPage
     * @param records 转换过的 List 模型
     * @param <T>     泛型
     * @return PageResult
     */
    public static <T> Page<T> toPage(Page<?> page, List<T> records) {
        Page<T> pageResult = new Page<>();
        pageResult.setPageNumber(page.getPageNumber());
        pageResult.setPageSize(page.getPageSize());
        pageResult.setTotalPage(page.getTotalPage());
        pageResult.setTotalRow(page.getTotalRow());
        pageResult.setRecords(records);
        return pageResult;
    }
}

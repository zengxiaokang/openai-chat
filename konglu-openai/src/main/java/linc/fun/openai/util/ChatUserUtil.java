package linc.fun.openai.util;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.NumberUtil;
import linc.fun.openai.constants.ApplicationConstants;
import linc.fun.openai.domain.bo.JwtUserInfoBO;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import lombok.experimental.UtilityClass;

/**
 * @author linc
 * @date 2023-4-16
 * 前端用户工具类
 */
@UtilityClass
public class ChatUserUtil {

    /**
     * 获取用户 id
     *
     * @return 用户 id
     */
    public Long getUserId() {
        return NumberUtil.parseLong(String.valueOf(StpUtil.getLoginId()));
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    public JwtUserInfoBO getJwtUserInfo() {
        JwtUserInfoBO userInfoBO = new JwtUserInfoBO();
        String registerTypeCode = (String) StpUtil.getExtra(ApplicationConstants.FRONT_JWT_REGISTER_TYPE_CODE);
        ChatUserRegisterTypeEnum registerType = ChatUserRegisterTypeEnum.CODE_MAP.get(registerTypeCode);
        userInfoBO.setRegisterType(registerType);
        userInfoBO.setUsername(String.valueOf(StpUtil.getExtra(ApplicationConstants.FRONT_JWT_USERNAME)));
        userInfoBO.setUserId(getUserId());
        userInfoBO.setUserBindingEmailId(NumberUtil.parseLong(String.valueOf(StpUtil.getExtra(ApplicationConstants.FRONT_JWT_EXTRA_USER_ID))));
        return userInfoBO;
    }
}

package linc.fun.openai.util;

public class ByteUtil {
	//原始数组
	byte[] bytes;
	//记录当前写入到多少位
	int index;

	private ByteUtil(int capacity){
		bytes = new byte[capacity];
		index = 0;
	}

	public static ByteUtil createBytes(int capacity){
		return new ByteUtil(capacity);
	}

	//向数组中追加内容
	public ByteUtil appendNumber(long val){
		byte[] arr = number2byte(val);
		appendBytes(arr);
		return this;
	}
	public ByteUtil appendNumber(int val){
		byte[] arr = number2byte(val);
		appendBytes(arr);
		return this;
	}
	public ByteUtil appendNumber(short val){
		byte[] arr = number2byte(val);
		appendBytes(arr);
		return this;
	}
	public ByteUtil appendNumber(byte val){
		byte[] arr = new byte[]{val};
		appendBytes(arr);
		return this;
	}

	/**
	 * 获取数据的总和
	 * @return
	 */
	public int getSum(){
		int ret = 0;
		for (byte aByte : bytes) {
			ret += aByte;
		}
		return ret;
	}

	// 追加byte数组
	public ByteUtil appendBytes(byte[] arr){
		for(byte i = 0 ; i < arr.length ; i ++){
			bytes[index + i] = arr[i];
		}
		index += arr.length;
		return this;
	}

	/**
     * 将数字转换为byte数组
     */
    public static byte[] number2byte(long val) {
		return new byte[]{
				(byte) ((val >> 56) & 0xFF),
				(byte) ((val >> 48) & 0xFF),
				(byte) ((val >> 40) & 0xFF),
				(byte) ((val >> 32) & 0xFF),
				(byte) ((val >> 24) & 0xFF),
				(byte) ((val >> 16) & 0xFF),
				(byte) ((val >> 8) & 0xFF),
				(byte) (val & 0xFF)
		};
    }
    public static byte[] number2byte(int val) {
		return new byte[]{
				(byte) ((val >> 24) & 0xFF),
				(byte) ((val >> 16) & 0xFF),
				(byte) ((val >> 8) & 0xFF),
				(byte) (val & 0xFF)
		};
    }
    public static byte[] number2byte(short val) {
		return new byte[]{
				(byte) ((val >> 8) & 0xFF),
				(byte) (val & 0xFF)
		};
    }
}

package linc.fun.openai.enums;


import com.fasterxml.jackson.annotation.JsonValue;
import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * API 类型枚举
 */
@AllArgsConstructor
public enum ApiTypeEnum {

    /**
     * API_KEY
     */
    API_KEY("ApiKey", "ChatGPTAPI"),

    /**
     * ACCESS_TOKEN
     */
    ACCESS_TOKEN("AccessToken", "ChatGPTUnofficialProxyAPI"),
    AlipayGo("Alipay", "AlipayGoServerAPI");

    @Getter
    @EnumValue
    private final String name;

    @Getter
    @JsonValue
    private final String message;
}

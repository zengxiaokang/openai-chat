package linc.fun.openai.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author hncboy
 * @date 2023/3/25 01:01
 * AccessToken 对话模型
 */
@AllArgsConstructor
public enum ChatConversationModelEnum {

    GPT_3_5_TURBO("gpt-3.5-turbo"),
    GPT_3_5_TURBO_0301("gpt-3.5-turbo-0301"),
    GPT_4("gpt-4"),
    GPT_4_0314("gpt-4-0314"),
    GPT_4_32K("gpt-4-32k"),
    GPT_4_32K_0314("gpt-4-32k-0314");

    @Getter
    @JsonValue
    private final String name;

    /**
     * name 作为 key，封装为 Map
     */
    public static final Map<String, ChatConversationModelEnum> NAME_MAP = Stream
            .of(ChatConversationModelEnum.values())
            .collect(Collectors.toMap(ChatConversationModelEnum::getName, Function.identity()));
}

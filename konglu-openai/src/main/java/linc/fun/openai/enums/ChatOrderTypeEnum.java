package linc.fun.openai.enums;

import com.mybatisflex.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yqlin
 * @date 2023/5/8 12:53
 * @description
 */
@AllArgsConstructor
public enum ChatOrderTypeEnum {
    /**
     * 订单类型(0:正常订单 1:秒杀订单)
     */
    NORMAL(0, "正常订单"),
    SEC_KILL(1, "秒杀订单");
    @Getter
    @EnumValue
    @JsonValue
    private final Integer code;
    @Getter
    private final String desc;
}

package linc.fun.openai.enums;

import com.mybatisflex.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yqlin
 * @date 2023/5/8 12:55
 * @description
 */
@AllArgsConstructor
public enum ChatOrderPayStatusEnum {
    /**
     * 支付状态 0:支付中(0:待付款 1:待确认) 1:支付成功(2:已完成)  2:支付失败(3:无效订单) 3.支付关闭
     */
    PAYMENT_PENDING(0, "支付中"),
    PAYMENT_SUCCESS(1, "支付成功"),
    PAYMENT_FAILED(2, "支付失败"),
    PAYMENT_CLOSED(3, "支付关闭"),

    ;
    @Getter
    @EnumValue
    @JsonValue
    private final Integer code;
    @Getter
    private final String desc;
}

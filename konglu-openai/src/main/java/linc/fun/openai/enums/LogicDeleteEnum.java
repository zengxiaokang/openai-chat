package linc.fun.openai.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.mybatisflex.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yqlin
 * @date 2023/5/13 12:17
 * @description
 */
@AllArgsConstructor
public enum LogicDeleteEnum {
    /**
     * 启用
     */
    DELETE(1),

    /**
     * 停用
     */
    NORMAL(0);

    @Getter
    @EnumValue
    @JsonValue
    private final Integer code;
}

package linc.fun.openai.enums;

import com.mybatisflex.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author yqlin
 * @date 2023/5/8 12:48
 * @description
 */
@AllArgsConstructor
public enum ChatOrderStatusEnum {
    /**
     * 订单状态(0:待付款 1:待确认 2:已完成 3:无效订单 4已关闭
     */
    PENDING_PAYMENT(0, "待付款"),
    PENDING_CONFIRMATION(1, "待确认"),
    COMPLETED(2, "已完成"),
    INVALID(3, "无效订单"),
    // 已关闭包含未支付订单、全额退款成功的订单
    CLOSED(4, "已关闭"),

    ;


    @Getter
    @EnumValue
    @JsonValue
    private final Integer code;
    @Getter
    private final String desc;
}

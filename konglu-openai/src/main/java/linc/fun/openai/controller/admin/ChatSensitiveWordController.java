package linc.fun.openai.controller.admin;

import com.mybatisflex.core.paginate.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.query.SensitiveWordPageQuery;
import linc.fun.openai.domain.vo.SensitiveWordVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatSensitiveWordService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author linc
 * @date 2023-3-28
 * 敏感词相关接口
 */
@Tag(name = "管理端-敏感词相关")
@ApiRestController( value = "/admin/sensitive_word")
public record ChatSensitiveWordController(ChatSensitiveWordService sensitiveWordService) {

    @GetMapping("/page")
    @Operation(summary = "敏感词列表分页")
    public R<Page<SensitiveWordVO>> page(@Validated SensitiveWordPageQuery query) {
        return R.data(sensitiveWordService.pageSensitiveWord(query));
    }
}

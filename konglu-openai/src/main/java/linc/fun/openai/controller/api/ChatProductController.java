package linc.fun.openai.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.vo.ChatTop4ProductsVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatProductService;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author yqlin
 * @date 2023/5/14 20:23
 * @description
 */
@Tag(name = "商品相关")
@ApiRestController(value = "/api/chat_product")
public record ChatProductController (ChatProductService chatProductService){

    @Operation(summary = "获取Top4商品")
    @GetMapping("/get_top4_products")
    public R<ChatTop4ProductsVO> getTop4Products(){
        return R.data(chatProductService.getTop4Products());
    }
}

package linc.fun.openai.controller.api;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.vo.CaptchaVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.helper.CaptchaHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * @author yqlin
 * @date 2023/3/6 13:50
 * @description
 */
@Tag(name = "验证码相关")
@Slf4j
@ApiRestController("/api/captcha")
public record CaptchaController(CaptchaHelper captchaHelper) {


    @Operation(summary = "获取图片验证码")
    @GetMapping("/get_pic_code")
    public R<CaptchaVO> getImageCaptcha() {
        return R.data(captchaHelper.generateCaptcha());
    }


}

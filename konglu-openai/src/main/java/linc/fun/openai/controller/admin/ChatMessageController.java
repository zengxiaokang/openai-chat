package linc.fun.openai.controller.admin;

import com.mybatisflex.core.paginate.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.query.ChatMessagePageQuery;
import linc.fun.openai.domain.vo.ChatMessageVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatMessageService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author linc
 * @date 2023-3-27
 * 聊天记录相关接口
 */
@Tag(name = "管理端-聊天记录相关")
@ApiRestController(value = "/admin/chat_message")
public record ChatMessageController(ChatMessageService chatMessageService) {

    @Operation(summary = "分页列表")
    @PostMapping("/page")
    public R<Page<ChatMessageVO>> page(@Validated @RequestBody ChatMessagePageQuery query) {
        return R.data(chatMessageService.pageChatMessage(query));
    }
}

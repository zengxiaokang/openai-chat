package linc.fun.openai.controller.admin;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.request.SysUserLoginRequest;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.SysUserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author linc
 * @date 2023-3-28
 * 系统用户相关接口
 */
@Tag(name = "管理端-系统用户相关")
@ApiRestController(value = "/admin/sys_user")
public record SysUserController(SysUserService sysUserService) {

    @Operation(summary = "用户登录")
    @PostMapping("/login")
    public R<Void> login(@Validated @RequestBody SysUserLoginRequest sysUserLoginRequest) {
        sysUserService.login(sysUserLoginRequest);
        return R.success("登录成功");
    }
}

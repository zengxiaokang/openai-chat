package linc.fun.openai.controller.api;

import cn.hutool.core.date.DateUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.request.ChatProcessRequest;
import linc.fun.openai.service.ChatMessageService;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

/**
 * @author linc
 * @date 2023-3-22
 * 聊天相关接口
 */
@Tag(name = "聊天相关")
@ApiRestController("/api/chat_message")
public record ChatMessageController(ChatMessageService chatMessageService) {

    @Operation(summary = "发送消息")
    @PostMapping("/send")
    public ResponseBodyEmitter sendMessage(@RequestBody @Validated ChatProcessRequest chatProcessRequest, HttpServletResponse response) {
        // TODO 后续调整
        chatProcessRequest.setSystemMessage("You are ChatGPT, a large language model trained by OpenAI. Answer as concisely as possible.\\nKnowledge cutoff: 2021-09-01\\nCurrent date: ".concat(DateUtil.today()));
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        return chatMessageService.sendMessage(chatProcessRequest);
    }
}

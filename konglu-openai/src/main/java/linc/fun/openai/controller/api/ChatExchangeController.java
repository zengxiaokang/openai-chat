package linc.fun.openai.controller.api;

import com.mybatisflex.core.paginate.Page;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.constraints.Size;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.query.MyExchangesPageQuery;
import linc.fun.openai.domain.entity.chat.ChatExchangeDO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatExchangeService;
import linc.fun.openai.util.ObjectMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author yqlin
 * @date 2023/5/17 23:36
 * @description
 */
@Slf4j
@ApiRestController("/api/chat_exchange")
public record ChatExchangeController(ChatExchangeService chatExchangeService) {


    /**
     * 立即兑换聊天次数
     */
    @PostMapping("/immediate_exchange_conversation")
    public R<Integer> immediateExchangeConversation(@RequestParam @Validated @Size(min = 20, max = 20, message = "兑换码为20位字符") String code) {
        log.info("立即兑换聊天次数，入参：{}", code);
        return R.data(chatExchangeService.immediateExchangeConversation(code));
    }

    @PostMapping("/page_my_exchanges")
    @Operation(summary = "分页查询我的兑换码")
    public R<Page<ChatExchangeDO>> pageMyExchanges(@Validated @RequestBody MyExchangesPageQuery query) {
        log.info("分页查询我的兑换码，入参：{}", ObjectMapperUtil.toJson(query));
        return R.data(chatExchangeService.pageMyExchanges(query));
    }

}

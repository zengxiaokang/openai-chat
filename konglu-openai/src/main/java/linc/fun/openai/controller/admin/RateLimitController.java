package linc.fun.openai.controller.admin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.vo.RateLimitVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.RateLimitService;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author linc
 * @date 2023-4-1
 * 限流记录相关接口
 */
@Tag(name = "管理端-限流记录相关")
@ApiRestController(value = "/admin/rate_limit")
public record RateLimitController(RateLimitService rateLimitService) {


    @Operation(summary = "限流列表")
    @GetMapping("/list")
    public R<List<RateLimitVO>> listRateLimit() {
        return R.data(rateLimitService.listRateLimit());
    }
}

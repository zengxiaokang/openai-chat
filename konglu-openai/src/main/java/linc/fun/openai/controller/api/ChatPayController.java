package linc.fun.openai.controller.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.request.AlipayFace2faceTradeRequest;
import linc.fun.openai.domain.vo.AlipayFace2faceTradeResponse;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatPayService;
import linc.fun.openai.util.ObjectMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author yqlin
 * @date 2023/5/5 22:10
 * @description
 */
@Tag(name = "支付相关")
@Slf4j
@ApiRestController("/api/chat_pay")
public record ChatPayController(ChatPayService aliPayService) {

    @PostMapping("/alipay/face2face_pay")
    @Operation(summary = "当面付")
    public R<AlipayFace2faceTradeResponse> alipayFace2facePay(@RequestBody @Validated AlipayFace2faceTradeRequest req) {
        log.info("当面付，入参: {}", ObjectMapperUtil.toJson(req));
        AlipayFace2faceTradeResponse responseVO = aliPayService.face2facePay(req);
        return R.data(responseVO);
    }

}

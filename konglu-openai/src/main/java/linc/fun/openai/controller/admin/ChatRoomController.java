package linc.fun.openai.controller.admin;

import com.mybatisflex.core.paginate.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.query.ChatRoomPageQuery;
import linc.fun.openai.domain.vo.ChatRoomVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatRoomService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author linc
 * @date 2023-3-27
 * 聊天室相关接口
 */
@Tag(name = "管理端-聊天室相关")
@ApiRestController(value = "/admin/chat_room")
public record ChatRoomController(ChatRoomService chatRoomService) {

    @Operation(summary = "聊天室分页列表")
    @PostMapping("/page")
    public R<Page<ChatRoomVO>> page(@Validated @RequestBody ChatRoomPageQuery query) {
        return R.data(chatRoomService.pageChatRoom(query));
    }
}

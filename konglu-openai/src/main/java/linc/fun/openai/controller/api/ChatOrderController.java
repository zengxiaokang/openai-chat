package linc.fun.openai.controller.api;

import com.mybatisflex.core.paginate.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import linc.fun.openai.annotation.ApiRestController;
import linc.fun.openai.domain.dto.query.MyOrdersPageQuery;
import linc.fun.openai.domain.dto.request.ChatProductTradeRequest;
import linc.fun.openai.domain.vo.MyOrderVO;
import linc.fun.openai.domain.vo.response.R;
import linc.fun.openai.service.ChatOrderService;
import linc.fun.openai.util.ObjectMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author yqlin
 * @date 2023/5/8 00:10
 * @description
 */
@Slf4j
@Tag(name = "订单相关")
@ApiRestController("/api/chat_order")
public record ChatOrderController(ChatOrderService chatOrderService) {

    @PostMapping("/create_order")
    @Operation(summary = "当面付预下订单")
    public R<String> createOrder(@RequestBody @Validated ChatProductTradeRequest req) {
        log.info("当面付预下订单，入参: {}", ObjectMapperUtil.toJson(req));
        return R.data(chatOrderService.createOrder(req).toString());
    }

    @GetMapping("/check_order_status_finished/{orderId}")
    @Operation(summary = "检查订单是否支付完成")
    public R<Boolean> checkOrderStatusFinished(@PathVariable Long orderId) {
        log.info("检查订单是否支付完成，入参: {}", ObjectMapperUtil.toJson(orderId));
        return R.data(chatOrderService.checkOrderStatusFinished(orderId));
    }

    @PostMapping("/page_my_orders")
    @Operation(summary = "分页查询我的订单")
    public R<Page<MyOrderVO>> pageMyOrders(@Validated @RequestBody MyOrdersPageQuery query) {
        log.info("分页查询我的订单，入参: {}", ObjectMapperUtil.toJson(query));
        return R.data(chatOrderService.pageMyOrders(query));
    }

    @PutMapping("/close_my_order/{orderId}")
    @Operation(summary = "关闭我的订单")
    public R<Void> closeMyOrder(@PathVariable("orderId") Long orderId) {
        log.info("关闭我的订单，入参: {}", orderId);
        chatOrderService.closeMyOrder(orderId);
        return R.success();
    }

    // todo: 处理异常订单，需要人工去补发

}

package linc.fun.openai.handler.converter;


import linc.fun.openai.domain.entity.chat.ChatMessageDO;
import linc.fun.openai.domain.vo.ChatMessageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author linc
 * @date 2023-3-28
 * 聊天记录相关转换
 */
@Mapper
public interface ChatMessageConverter {

    ChatMessageConverter INSTANCE = Mappers.getMapper(ChatMessageConverter.class);

    /**
     * entityToVO
     *
     * @param chatMessageDOList chatMessageDOList
     * @return List<ChatMessageVO>
     */
    List<ChatMessageVO> entityToVO(List<ChatMessageDO> chatMessageDOList);
}

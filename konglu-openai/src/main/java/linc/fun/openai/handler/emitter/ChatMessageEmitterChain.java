package linc.fun.openai.handler.emitter;

import cn.hutool.extra.spring.SpringUtil;
import linc.fun.openai.config.openai.OpenaiConfig;
import linc.fun.openai.domain.dto.request.ChatProcessRequest;
import linc.fun.openai.enums.ApiTypeEnum;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

/**
 * @author linc
 * @date 2023-3-29
 * 正常发送消息链路，最后一个节点
 */
public class ChatMessageEmitterChain extends AbstractResponseEmitterChain {

    @Override
    public void doChain(ChatProcessRequest request, ResponseBodyEmitter emitter) {
        ApiTypeEnum apiTypeEnum = SpringUtil.getBean(OpenaiConfig.class).getApiTypeEnum();
        ResponseEmitter responseEmitter;
        // 如果使用的是api—key
        if (apiTypeEnum == ApiTypeEnum.API_KEY) {
            responseEmitter = SpringUtil.getBean(ApiKeyResponseEmitter.class);
        } else {
            // 如果使用的是access-token
            responseEmitter = SpringUtil.getBean(AccessTokenResponseEmitter.class);
        }
        responseEmitter.requestToResponseEmitter(request, emitter);
    }
}

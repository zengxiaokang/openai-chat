package linc.fun.openai.handler.validation.impl;

import cn.hutool.core.util.NumberUtil;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import linc.fun.openai.handler.validation.annotation.BigDecimalStringMoney;

import java.math.BigDecimal;

/**
 * @author yqlin
 * @date 2023/5/5 22:17
 * @description
 */
public class BigDecimalStringMoneyImpl implements ConstraintValidator<BigDecimalStringMoney, String> {
    @Override
    public void initialize(BigDecimalStringMoney constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (!NumberUtil.isNumber(value)) {
            return false;
        }
        return new BigDecimal(value).compareTo(BigDecimal.ZERO) > 0;
    }


}

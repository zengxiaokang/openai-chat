package linc.fun.openai.handler.validation.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import linc.fun.openai.handler.validation.impl.BigDecimalStringMoneyImpl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author yqlin
 * @date 2023/5/5 22:17
 * @description
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BigDecimalStringMoneyImpl.class)
public @interface BigDecimalStringMoney {

    String message() default "金额必须为数值，并且必须大于0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

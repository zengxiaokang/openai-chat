package linc.fun.openai.handler.converter;

import linc.fun.openai.domain.entity.chat.ChatRoomDO;
import linc.fun.openai.domain.vo.ChatRoomVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author linc
 * @date 2023-3-27
 * 聊天室相关转换
 */
@Mapper
public interface ChatRoomConverter {

    ChatRoomConverter INSTANCE = Mappers.getMapper(ChatRoomConverter.class);

    /**
     * entityToVO
     *
     * @param chatRoomDOList chatRoomDOList
     * @return List<ChatRoomVO>
     */
    List<ChatRoomVO> entityToVO(List<ChatRoomDO> chatRoomDOList);
}

package linc.fun.openai.handler.converter;

import linc.fun.openai.domain.entity.chat.ChatSensitiveWordDO;
import linc.fun.openai.domain.vo.SensitiveWordVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author linc
 * @date 2023-3-28
 * 敏感词相关转换
 */
@Mapper
public interface SensitiveWordConverter {

    SensitiveWordConverter INSTANCE = Mappers.getMapper(SensitiveWordConverter.class);

    /**
     * entityToVO
     *
     * @param sensitiveWordDOList sensitiveWordDOList
     * @return List<SensitiveWordVO>
     */
    List<SensitiveWordVO> entityToVO(List<ChatSensitiveWordDO> sensitiveWordDOList);
}

package linc.fun.openai.handler.emitter;

import cn.hutool.extra.spring.SpringUtil;
import linc.fun.openai.domain.dto.request.ChatProcessRequest;
import linc.fun.openai.domain.entity.chat.ChatUserDO;
import linc.fun.openai.exception.BizException;
import linc.fun.openai.service.ChatUserService;
import linc.fun.openai.util.ChatUserUtil;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

/**
 * @author yqlin
 * @date 2023/5/4 21:50
 * @description
 */
public class ChatUserUsageChain extends AbstractResponseEmitterChain {



    @Override
    public void doChain(ChatProcessRequest request, ResponseBodyEmitter emitter) {
        ChatUserService chatUserService = SpringUtil.getBean(ChatUserService.class);
        // 判断当前用户的使用次数是否足够
        ChatUserDO user = chatUserService.getById(ChatUserUtil.getUserId());
        if (user.getCanConversationTimes() == 0) {
            throw BizException.of("您的聊天次数已用完，可以前往【立即充值】进行充值");
        }
        if (getNext() != null) {
            getNext().doChain(request, emitter);
        }
    }
}

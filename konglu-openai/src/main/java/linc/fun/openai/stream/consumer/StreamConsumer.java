package linc.fun.openai.stream.consumer;

import linc.fun.openai.domain.dto.mq.StreamMessage;
import org.springframework.messaging.Message;

import java.util.function.Consumer;

/**
 * @author yqlin
 * @date 2023/5/11 14:23
 * @description
 */
public interface StreamConsumer {
    Consumer<Message<StreamMessage>> consume();
}

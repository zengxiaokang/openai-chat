package linc.fun.openai.stream.producer;

import linc.fun.openai.domain.dto.mq.StreamMessage;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author yqlin
 * @date 2023/5/11 14:06
 * @description
 */

@Component
public record StreamProducer(StreamBridge streamBridge) {
    /**
     * 发送消息
     */
    public boolean sendMessage(String msgText, String bindingName) {
        // 构建消息对象
        StreamMessage messaging = new StreamMessage();
        messaging.setMessageId(UUID.randomUUID().toString());
        messaging.setMessageText(msgText);
        Message<StreamMessage> message = MessageBuilder.withPayload(messaging).build();
        return streamBridge.send(bindingName, message);
    }

    /**
     * 发送延迟消息
     */
    public boolean sendDelayMessage(String msgText, String bindingName, Integer seconds) {
        // 构建消息对象
        StreamMessage messaging = new StreamMessage();
        messaging.setMessageId(UUID.randomUUID().toString());
        messaging.setMessageText(msgText);
        Message<StreamMessage> message = MessageBuilder.withPayload(messaging)
                .setHeader("x-delay", seconds * 1000)
                .build();
        return streamBridge.send(bindingName, message);
    }
}

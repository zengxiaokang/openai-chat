package linc.fun.openai.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import linc.fun.openai.domain.entity.chat.ChatOrderItemDO;
import linc.fun.openai.mapper.ChatOrderItemMapper;
import linc.fun.openai.service.ChatOrderItemService;
import org.springframework.stereotype.Service;

/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
@Service
public class ChatOrderItemServiceImpl extends ServiceImpl<ChatOrderItemMapper, ChatOrderItemDO> implements ChatOrderItemService {

}

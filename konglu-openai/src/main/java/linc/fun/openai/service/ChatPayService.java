package linc.fun.openai.service;

import linc.fun.openai.domain.dto.request.AlipayFace2faceTradeRequest;
import linc.fun.openai.domain.vo.AlipayFace2faceTradeResponse;

/**
 * @author yqlin
 * @date 2023/5/6 11:29
 * @description
 */
public interface ChatPayService {
    AlipayFace2faceTradeResponse face2facePay(AlipayFace2faceTradeRequest request);
}

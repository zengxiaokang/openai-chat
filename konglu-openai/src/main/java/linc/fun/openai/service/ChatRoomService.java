package linc.fun.openai.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.query.ChatRoomPageQuery;
import linc.fun.openai.domain.entity.chat.ChatMessageDO;
import linc.fun.openai.domain.entity.chat.ChatRoomDO;
import linc.fun.openai.domain.vo.ChatRoomVO;


/**
 * @author linc
 * @date 2023-3-27
 * 聊天室相关业务接口
 */
public interface ChatRoomService extends IService<ChatRoomDO> {


    /**
     * 聊天室分页
     *
     * @param query 查询参数
     * @return 聊天室展示参数
     */
    Page<ChatRoomVO> pageChatRoom(ChatRoomPageQuery query);


    /**
     * 创建聊天室
     *
     * @param chatMessageDO 聊天记录
     * @return 聊天室
     */
    ChatRoomDO createChatRoom(ChatMessageDO chatMessageDO);
}

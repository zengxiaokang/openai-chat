package linc.fun.openai.service;

import linc.fun.openai.domain.entity.chat.ChatOrderDelayFailureRecord;
import com.mybatisflex.core.service.IService;

/**
 * @author yqlin
 * @date 2023/5/11 18:42
 * @description
 */
public interface ChatOrderDelayFailureRecordService extends IService<ChatOrderDelayFailureRecord>{


}

package linc.fun.openai.service;

import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.entity.chat.ChatUserLoginLogDO;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;

/**
 * 前端用户登录日志业务接口
 *
 * @author CoDeleven
 */
public interface ChatUserLoginLogService extends IService<ChatUserLoginLogDO> {

    /**
     * 登录失败记录表
     *
     * @param registerType 注册类型
     * @param userBindingEmailId  登录信息表ID
     * @param userId   基础用户ID
     * @param message      失败原因
     */
    void loginFailed(ChatUserRegisterTypeEnum registerType, Long userBindingEmailId, Long userId, String message);

    /**
     * 登录成功记录表
     *
     * @param registerType 注册类型
     * @param userBindingEmailId  登录信息表ID
     * @param userId   基础用户ID
     */
    void loginSuccess(ChatUserRegisterTypeEnum registerType, Long userBindingEmailId, Long userId);
}

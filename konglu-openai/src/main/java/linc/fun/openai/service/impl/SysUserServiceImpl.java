package linc.fun.openai.service.impl;


import jakarta.annotation.Resource;
import linc.fun.openai.config.openai.OpenaiConfig;
import linc.fun.openai.domain.dto.request.SysUserLoginRequest;
import linc.fun.openai.exception.AuthException;
import linc.fun.openai.service.SysUserService;
import linc.fun.openai.util.StpAdminUtil;
import org.springframework.stereotype.Service;

/**
 * @author linc
 * @date 2023-3-28
 * 系统用户业务实现类
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private OpenaiConfig openaiConfig;

    @Override
    public void login(SysUserLoginRequest sysUserLoginRequest) {
        if (sysUserLoginRequest.getAccount().equals(openaiConfig.getAdminAccount()) &&
                sysUserLoginRequest.getPassword().equals(openaiConfig.getAdminPassword())) {
            StpAdminUtil.login(sysUserLoginRequest.getAccount());
            return;
        }
        throw new AuthException("账号或密码错误");
    }
}

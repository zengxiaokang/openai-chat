package linc.fun.openai.service;

import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.entity.chat.ChatUserDO;
import linc.fun.openai.domain.entity.chat.ChatUserBindingDO;
import linc.fun.openai.domain.entity.chat.ChatUserBindingEmailDO;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;


/**
 * 前端用户绑定相关业务接口
 *
 * @author CoDeleven
 */
public interface ChatUserBindingService extends IService<ChatUserBindingDO> {

    /**
     * 绑定邮箱
     *
     * @param baseUser     基础用户
     * @param extraEmailDO 邮件扩展信息
     */
    void bindEmail(ChatUserDO baseUser, ChatUserBindingEmailDO extraEmailDO);

    /**
     * 找到绑定关系
     *
     * @param frontUserRegisterTypeEnum 注册类型
     * @param userBindingEmailId        扩展信息 id
     * @return 绑定关系
     */
    ChatUserBindingDO findBindingEmail(ChatUserRegisterTypeEnum frontUserRegisterTypeEnum, Long userBindingEmailId);
}

package linc.fun.openai.service;

import linc.fun.openai.domain.entity.chat.ChatUserExchangeRecordDO;
import com.mybatisflex.core.service.IService;

/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description
 */
public interface ChatUserExchangeRecordService extends IService<ChatUserExchangeRecordDO>{


}

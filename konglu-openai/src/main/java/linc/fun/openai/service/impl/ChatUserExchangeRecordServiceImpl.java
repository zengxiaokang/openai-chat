package linc.fun.openai.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import linc.fun.openai.domain.entity.chat.ChatUserExchangeRecordDO;
import linc.fun.openai.mapper.ChatUserExchangeRecordMapper;
import linc.fun.openai.service.ChatUserExchangeRecordService;
import org.springframework.stereotype.Service;

/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description
 */
@Service
public class ChatUserExchangeRecordServiceImpl extends ServiceImpl<ChatUserExchangeRecordMapper, ChatUserExchangeRecordDO> implements ChatUserExchangeRecordService {

}

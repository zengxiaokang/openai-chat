package linc.fun.openai.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import linc.fun.openai.domain.bo.JwtUserInfoBO;
import linc.fun.openai.domain.dto.request.RegisterFrontUserForEmailRequest;
import linc.fun.openai.domain.entity.chat.ChatUserDO;
import linc.fun.openai.domain.vo.LoginInfoVO;
import linc.fun.openai.domain.vo.RegisterCaptchaVO;
import linc.fun.openai.domain.vo.UserInfoVO;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import linc.fun.openai.mapper.ChatUserMapper;
import linc.fun.openai.service.ChatUserService;
import linc.fun.openai.strategy.user.AbstractRegisterTypeStrategy;
import linc.fun.openai.util.ChatUserUtil;
import linc.fun.openai.util.IdGenerator;
import linc.fun.openai.util.SimpleCaptchaUtil;
import linc.fun.openai.util.WebUtil;
import org.springframework.stereotype.Service;

/**
 * 基础用户业务实现类
 *
 * @author CoDeleven
 */
@Service
public class ChatUserServiceImpl extends ServiceImpl<ChatUserMapper, ChatUserDO> implements ChatUserService {

    @Resource
    private IdGenerator idGenerator;

    @Override
    public ChatUserDO createEmptyUser() {
        ChatUserDO chatUser = new ChatUserDO();
        chatUser.setNickname("XinyuAi_" + RandomUtil.randomString(6));
        chatUser.setId(idGenerator.getId());
        chatUser.setLastLoginIp(null);
        chatUser.setDescription(null);
        chatUser.setAvatarVersion(0);
        this.save(chatUser);
        return chatUser;
    }

    @Override
    public ChatUserDO findUserInfoById(Long userId) {
        return this.getById(userId);
    }

    @Override
    public void updateLastLoginIp(Long userId) {
        ChatUserDO user = new ChatUserDO();
        user.setId(userId);
        user.setLastLoginIp(WebUtil.getIp());
        this.updateById(user);
    }


    @Override
    public void register(RegisterFrontUserForEmailRequest request) {
        AbstractRegisterTypeStrategy registerStrategy = AbstractRegisterTypeStrategy.findStrategyByRegisterType(request.getRegisterType());
        registerStrategy.register(request);
    }

    @Override
    public void verifyCode(ChatUserRegisterTypeEnum registerType, String code) {
        AbstractRegisterTypeStrategy registerStrategy = AbstractRegisterTypeStrategy.findStrategyByRegisterType(registerType);
        registerStrategy.checkVerifyCode(null, code);
    }

    @Override
    public LoginInfoVO login(ChatUserRegisterTypeEnum registerType, String username, String password) {
        AbstractRegisterTypeStrategy strategy = AbstractRegisterTypeStrategy.findStrategyByRegisterType(registerType);
        return strategy.login(username, password);
    }

    @Override
    public UserInfoVO getUserInfo(ChatUserRegisterTypeEnum registerType, Long userBindingEmailId) {
        AbstractRegisterTypeStrategy strategy = AbstractRegisterTypeStrategy.findStrategyByRegisterType(registerType);
        return strategy.getLoginUserInfo(userBindingEmailId);
    }

    @Override
    public RegisterCaptchaVO generateCaptcha() {
        // 创建一个 图形验证码会话ID
        String picCodeSessionId = IdUtil.fastUUID();
        // 根据图形验证码会话ID获取一个图形验证码。该方法会建立起 验证码会话ID 和 图形验证码的关系
        String captchaBase64Image = SimpleCaptchaUtil.getBase64Captcha(picCodeSessionId);
        // 将验证码会话ID加入到Cookie中
        return RegisterCaptchaVO.builder()
                .picCodeSessionId(picCodeSessionId)
                .picCodeBase64(captchaBase64Image).build();
    }

    @Override
    public UserInfoVO getUserInfo() {
        JwtUserInfoBO jwtUserInfo = ChatUserUtil.getJwtUserInfo();
        return this.getUserInfo(jwtUserInfo.getRegisterType(), jwtUserInfo.getUserBindingEmailId());
    }
}

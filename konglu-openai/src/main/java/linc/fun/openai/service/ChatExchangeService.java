package linc.fun.openai.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.query.MyExchangesPageQuery;
import linc.fun.openai.domain.entity.chat.ChatExchangeDO;


/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description
 */
public interface ChatExchangeService extends IService<ChatExchangeDO> {


    Integer immediateExchangeConversation(String code);

    Page<ChatExchangeDO> pageMyExchanges(MyExchangesPageQuery query);
}

package linc.fun.openai.service.impl;

import com.mybatisflex.spring.service.impl.ServiceImpl;
import linc.fun.openai.domain.entity.chat.ChatOrderHistoryDO;
import linc.fun.openai.mapper.ChatOrderHistoryMapper;
import linc.fun.openai.service.ChatOrderHistoryService;
import org.springframework.stereotype.Service;

/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
@Service
public class ChatOrderHistoryServiceImpl extends ServiceImpl<ChatOrderHistoryMapper, ChatOrderHistoryDO> implements ChatOrderHistoryService{

}

package linc.fun.openai.service;


import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.entity.chat.ChatProductDO;
import linc.fun.openai.domain.vo.ChatTop4ProductsVO;

/**
 * @author yqlin
 * @date 2023/5/8 00:01
 * @description
 */
public interface ChatProductService  extends IService<ChatProductDO> {


    ChatTop4ProductsVO getTop4Products();
}

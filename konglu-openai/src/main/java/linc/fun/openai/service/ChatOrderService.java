package linc.fun.openai.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.query.MyOrdersPageQuery;
import linc.fun.openai.domain.dto.request.ChatProductTradeRequest;
import linc.fun.openai.domain.entity.chat.ChatOrderDO;
import linc.fun.openai.domain.vo.MyOrderVO;

/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
public interface ChatOrderService extends IService<ChatOrderDO> {

    Long createOrder(ChatProductTradeRequest req);

    Boolean checkOrderStatusFinished(Long orderId);

    Page<MyOrderVO> pageMyOrders(MyOrdersPageQuery query);

    void closeMyOrder(Long orderId);
}

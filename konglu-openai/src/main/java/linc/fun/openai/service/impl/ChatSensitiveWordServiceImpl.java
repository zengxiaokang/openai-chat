package linc.fun.openai.service.impl;

import cn.hutool.core.util.StrUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import linc.fun.openai.domain.dto.query.SensitiveWordPageQuery;
import linc.fun.openai.domain.entity.chat.ChatSensitiveWordDO;
import linc.fun.openai.domain.vo.SensitiveWordVO;
import linc.fun.openai.handler.converter.SensitiveWordConverter;
import linc.fun.openai.mapper.ChatSensitiveWordMapper;
import linc.fun.openai.service.ChatSensitiveWordService;
import linc.fun.openai.util.PageUtil;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static linc.fun.openai.domain.entity.chat.table.Tables.ChatSensitiveWord;

/**
 * @author linc
 * @date 2023-3-28
 * 敏感词业务实现类
 */
@Service
public class ChatSensitiveWordServiceImpl extends ServiceImpl<ChatSensitiveWordMapper, ChatSensitiveWordDO> implements ChatSensitiveWordService {

    @Override
    public Page<SensitiveWordVO> pageSensitiveWord(SensitiveWordPageQuery query) {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .orderBy(ChatSensitiveWord.CreateTime.desc())
                .where(ChatSensitiveWord.Status.eq(query.getStatus()).when(Objects.nonNull(query.getStatus())))
                .and(ChatSensitiveWord.Word.like(query.getWord()).when(StrUtil.isNotBlank(query.getWord())));
        Page<ChatSensitiveWordDO> page = this.page(new Page<>(query.getPageNum(), query.getPageNum()), queryWrapper);
        return PageUtil.toPage(page, SensitiveWordConverter.INSTANCE.entityToVO(page.getRecords()));
    }
}

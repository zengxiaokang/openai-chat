package linc.fun.openai.service.impl;

import org.springframework.stereotype.Service;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import linc.fun.openai.mapper.ChatOrderDelayFailureRecordMapper;
import linc.fun.openai.domain.entity.chat.ChatOrderDelayFailureRecord;
import linc.fun.openai.service.ChatOrderDelayFailureRecordService;

/**
 * @author yqlin
 * @date 2023/5/11 18:42
 * @description
 */
@Service
public class ChatOrderDelayFailureRecordServiceImpl extends ServiceImpl<ChatOrderDelayFailureRecordMapper, ChatOrderDelayFailureRecord> implements ChatOrderDelayFailureRecordService{

}

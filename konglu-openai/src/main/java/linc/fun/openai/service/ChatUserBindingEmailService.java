package linc.fun.openai.service;

import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.entity.chat.ChatUserBindingEmailDO;

/**
 * 前端用户邮箱扩展业务接口
 *
 * @author CoDeleven
 */
public interface ChatUserBindingEmailService extends IService<ChatUserBindingEmailDO> {

    /**
     * 是否已使用
     *
     * @param username 邮箱
     * @return 是否已使用，true已使用；false未使用
     */
    boolean isUsed(String username);

    /**
     * 获取一个未被验证的邮箱账号信息
     *
     * @param identity 邮箱
     * @return 邮箱信息
     */
    ChatUserBindingEmailDO getUnverifiedEmailAccount(String identity);

    /**
     * 根据用户名获取邮箱账号信息
     *
     * @param username 邮箱
     * @return 邮箱信息
     */
    ChatUserBindingEmailDO getEmailAccount(String username);

    /**
     * 邮件验证完毕
     *
     * @param userBindingEmail 邮箱登录信息
     */
    void verifySuccess(ChatUserBindingEmailDO userBindingEmail);
}

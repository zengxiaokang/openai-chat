package linc.fun.openai.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.query.ChatMessagePageQuery;
import linc.fun.openai.domain.dto.request.ChatProcessRequest;
import linc.fun.openai.domain.entity.chat.ChatMessageDO;
import linc.fun.openai.domain.vo.ChatMessageVO;
import linc.fun.openai.enums.ApiTypeEnum;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

/**
 * @author linc
 * @date 2023-3-27
 * 聊天记录相关业务接口
 */
public interface ChatMessageService extends IService<ChatMessageDO> {

    /**
     * 聊天记录分页
     *
     * @param query 查询参数
     * @return 聊天记录展示参数
     */
    Page<ChatMessageVO> pageChatMessage(ChatMessagePageQuery query);


    /**
     * 消息处理
     *
     * @param chatProcessRequest 消息处理请求参数
     * @return emitter
     */
    ResponseBodyEmitter sendMessage(ChatProcessRequest chatProcessRequest);

    /**
     * 初始化聊天消息
     *
     * @param chatProcessRequest 消息处理请求参数
     * @param apiTypeEnum        API 类型
     * @return 聊天消息
     */
    ChatMessageDO initChatMessage(ChatProcessRequest chatProcessRequest, ApiTypeEnum apiTypeEnum);
}

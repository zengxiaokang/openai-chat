package linc.fun.openai.service;

import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.request.RegisterFrontUserForEmailRequest;
import linc.fun.openai.domain.entity.chat.ChatUserDO;
import linc.fun.openai.domain.vo.LoginInfoVO;
import linc.fun.openai.domain.vo.RegisterCaptchaVO;
import linc.fun.openai.domain.vo.UserInfoVO;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;

/**
 * 前端用户基础用户业务接口
 *
 * @author CoDeleven
 */
public interface ChatUserService extends IService<ChatUserDO> {

    /**
     * 创建一个空的基础用户信息
     *
     * @return 用户信息
     */
    ChatUserDO createEmptyUser();

    /**
     * 获取基础用户信息
     *
     * @param userId 基础用户 id
     * @return 用户信息
     */
    ChatUserDO findUserInfoById(Long userId);

    /**
     * 更新上次登录 ip
     *
     * @param chatUserId 基础用户 id
     */
    void updateLastLoginIp(Long chatUserId);


    /**
     * 处理注册请求
     *
     * @param request 注册请求
     */
    void register(RegisterFrontUserForEmailRequest request);

    /**
     * 验证码验证
     *
     * @param frontUserRegisterTypeEnum 注册类型
     * @param code                      验证码，可以是邮箱也可以是手机
     */
    void verifyCode(ChatUserRegisterTypeEnum frontUserRegisterTypeEnum, String code);

    /**
     * 执行登录
     *
     * @param registerType 注册类型
     * @param username     登录用户名，可以是邮箱，可以是手机
     * @param password     登录口令
     * @return Sa-Token的登录结果
     */
    LoginInfoVO login(ChatUserRegisterTypeEnum registerType, String username, String password);

    /**
     * 根据注册类型+其他绑定信息表获取该用户的登录信息
     *
     * @param registerType       注册类型
     * @param userBindingEmailId 对应注册类型的表ID
     * @return 登录用户信息
     */
    UserInfoVO getUserInfo(ChatUserRegisterTypeEnum registerType, Long userBindingEmailId);

    /**
     * 根据当前登录的状态获取用户信息
     *
     * @return 登录的用户信息
     */
    UserInfoVO getUserInfo();

    /**
     * 生成基于 Base64 的图形验证码
     *
     * @return Base64 图形验证码
     */
    RegisterCaptchaVO generateCaptcha();
}

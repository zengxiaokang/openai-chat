package linc.fun.openai.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;
import linc.fun.openai.domain.dto.query.SensitiveWordPageQuery;
import linc.fun.openai.domain.entity.chat.ChatSensitiveWordDO;
import linc.fun.openai.domain.vo.SensitiveWordVO;


/**
 * @author linc
 * @date 2023-3-28
 * 敏感词业务接口
 */
public interface ChatSensitiveWordService extends IService<ChatSensitiveWordDO> {

    /**
     * 敏感词分页查询
     *
     * @param query 查询条件
     * @return 敏感词分页列表
     */
    Page<SensitiveWordVO> pageSensitiveWord(SensitiveWordPageQuery query);
}

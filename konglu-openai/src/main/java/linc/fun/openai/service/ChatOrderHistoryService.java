package linc.fun.openai.service;

import linc.fun.openai.domain.entity.chat.ChatOrderHistoryDO;
import com.mybatisflex.core.service.IService;

/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
public interface ChatOrderHistoryService extends IService<ChatOrderHistoryDO> {


}

package linc.fun.openai.service;


import linc.fun.openai.domain.vo.RateLimitVO;

import java.util.List;

/**
 * @author linc
 * @date 2023-4-1
 * 限流记录业务接口
 */
public interface RateLimitService {

    /**
     * 查询限流列表
     *
     * @return 限流列表
     */
    List<RateLimitVO> listRateLimit();
}

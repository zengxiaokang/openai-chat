package linc.fun.openai.service.impl;

import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import linc.fun.openai.domain.entity.chat.ChatUserBindingDO;
import linc.fun.openai.domain.entity.chat.ChatUserBindingEmailDO;
import linc.fun.openai.domain.entity.chat.ChatUserDO;
import linc.fun.openai.enums.ChatUserBindingTypeEnum;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import linc.fun.openai.mapper.ChatUserBindingMapper;
import linc.fun.openai.service.ChatUserBindingService;
import linc.fun.openai.util.IdGenerator;
import org.springframework.stereotype.Service;

import static linc.fun.openai.domain.entity.chat.table.Tables.ChatUserBinding;

/**
 * 前端用户绑定相关业务实现类
 *
 * @author CoDeleven
 */
@Service
public class ChatUserBindingServiceImpl extends ServiceImpl<ChatUserBindingMapper, ChatUserBindingDO>
        implements ChatUserBindingService {
    @Resource
    private IdGenerator idGenerator;

    @Override
    public void bindEmail(ChatUserDO baseUser, ChatUserBindingEmailDO extraEmailDO) {
        ChatUserBindingDO userBinding = new ChatUserBindingDO();
        userBinding.setId(idGenerator.getId());
        userBinding.setBindingType(ChatUserBindingTypeEnum.BIND_EMAIL);
        userBinding.setUserBindingEmailId(extraEmailDO.getId());
        userBinding.setUserId(baseUser.getId());
        this.save(userBinding);
    }

    @Override
    public ChatUserBindingDO findBindingEmail(ChatUserRegisterTypeEnum registerType, Long userBindingEmailId) {
        return this.getOne(QueryWrapper.create()
                .where(ChatUserBinding.BindingType.eq(registerType))
                .and(ChatUserBinding.UserBindingEmailId.eq(userBindingEmailId)));
    }
}





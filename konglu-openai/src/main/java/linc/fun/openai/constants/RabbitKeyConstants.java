package linc.fun.openai.constants;

/**
 * @author yqlin
 * @date 2023/5/10 21:44
 * @description
 */
public interface RabbitKeyConstants {
    String CHAT_ORDER_DLQ_EXCHANGE = "chat-order-dlq-topic";
    String CHAT_ORDER_DLQ_BINDING_NAME = "chat-order-dlq-output";
    String RABBIT_STREAM_X_DELAY = "x-delay";
}


package linc.fun.openai.constants;

/**
 * @author linc
 * @date 2023-3-27
 * 应用相关常量
 */
public interface ApplicationConstants {

    /**
     * ADMIN 路径前缀
     */
    String ADMIN_PATH_PREFIX = "admin";

    /**
     * ADMIN 路径前缀
     */
    String API_PATH_PREFIX = "api";

    /**
     * 用户登录-JWT携带参数名称：注册类型 code
     */
    String FRONT_JWT_REGISTER_TYPE_CODE = "registerTypeCode";

    /**
     * 用户登录-JWT 携带参数名称：登录账号
     */
    String FRONT_JWT_USERNAME = "username";

    /**
     * 用户登录-JWT 携带参数名称：extraUserId
     */
    String FRONT_JWT_EXTRA_USER_ID = "extraUserId";

    String CHAT_ORDER_PURCHASE_SUBJECT = "忻宇ai聊天使用次数购买";
    String CHAT_ORDER_ACTIVE_CLOSE_ORDER_REMARK = "用户主动关闭待支付订单";
    String CHAT_ORDER_TIMEOUT_CLOSE_ORDER_REMARK = "超时主动关闭待支付订单";

    String CHAT_MESSAGE_SPLIT_KEY = "☉#$open_ai$#☉";
}

package linc.fun.openai.constants;

/**
 * @author yqlin
 * @date 2023/5/9 00:46
 * @description
 */
public interface RedisKeyConstants {

    String CHAT_USER_OCCUPY_EXCHANGE_IDS_KEY = "chat_user_occupy_exchange_ids_key:";

    String KEY_JOIN = ":";

    String CHAT_PRODUCT_ID_STOCK_KEY = "chat_product_id_stock_key";
    String SYNC_CHAT_PRODUCT_ID_STOCK_LOCK = "sync_chat_product_id_stock_lock";
    String DEDUCTION_CHAT_PRODUCT_STOCK_LOCK_KEY = "deduction_chat_product_stock_lock_key:";
    String INCREASE_CHAT_PRODUCT_STOCK_LOCK_KEY = "increase_chat_product_stock_lock_key:";
    String CHAT_ORDER_DELAY_CONSUMER_KEY = "chat_order_delay_consumer_key:";
    String CHAT_ORDER_RETRY_COUNT_KEY = "chat_order_retry_count_key:";
    String CHAT_USER_USAGE_LOCK_KEY = "chat_user_usage_lock_key";
}

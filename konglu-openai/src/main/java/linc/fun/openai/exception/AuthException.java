package linc.fun.openai.exception;

import linc.fun.openai.domain.vo.response.IResultCode;
import linc.fun.openai.domain.vo.response.ResultCode;
import lombok.Getter;

/**
 * @author linc
 * @date 2023-3-23
 * 鉴权异常
 */
public class AuthException extends RuntimeException {

    @Getter
    private final IResultCode resultCode;

    public AuthException(String message) {
        super(message);
        this.resultCode = ResultCode.UN_AUTHORIZED;
    }
}

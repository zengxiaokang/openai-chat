package linc.fun.openai.exception;

import linc.fun.openai.domain.vo.response.ResultCode;
import lombok.experimental.UtilityClass;

/**
 * @author yqlin
 * @date 2023/5/4 21:17
 * @description
 */
@UtilityClass
public class BizException {
    public RuntimeException QUESTION_TOO_LONG = new ServiceException("对话过长，请重新建立对话");
    public RuntimeException MESSAGE_SEND_ERROR = new ServiceException("消息发送异常");
    public RuntimeException MUST_HTTP_REQUEST = new ServiceException("必须是http请求");
    public RuntimeException CAPTCHA_UUID_BLANK = new ServiceException("图片验证码uuid为空");
    public RuntimeException CAPTCHA_CODE_BLANK = new ServiceException("图片验证码code为空");
    public RuntimeException CAPTCHA_ERROR = new ServiceException("验证码错误");
    public RuntimeException PARENT_MESSAGE_NOT_EXIST = new ServiceException("父级消息不存在，本次对话出错，请先关闭上下文或开启新的对话窗口");
    public RuntimeException CURRENT_ACCESS_TOKEN_AND_MODEL_NOT_MATCH = new ServiceException("当前对话类型为 AccessToken 使用模型不一样，请开启新的对话");
    public RuntimeException QUESTION_COUNT_HAS_LIMIT = new ServiceException("已达到上限，请关闭上下文对话重新发送");
    public RuntimeException CODE_NOT_EXIST_OR_EXPIRED = new ServiceException("验证码不存在或已过期，请重新发起...");
    public RuntimeException USERNAME_PASSWORD_ERROR = new ServiceException("账号或密码错误");
    public RuntimeException EMAIL_NOT_REGISTER = new ServiceException("邮箱未注册");
    public RuntimeException DATA_HAS_BEEN_LOOSED = new ServiceException("数据丢失，请联系管理员处理：18061877017");
    public RuntimeException CHAT_EXCHANGES_OUT_OF_STOCK = new ServiceException("兑换码暂无库存，请稍后再试试，或联系管理员：18061877017");
    public RuntimeException CHAT_PRODUCT_DEDUCTION_STOCK_ERROR = new ServiceException("商品扣减库存超时，请稍后再试试，或联系管理员：18061877017");
    public final RuntimeException INVALID_ORDER = new ServiceException("无效订单");
    public final RuntimeException UN_AUTHENTICATION = new ServiceException(ResultCode.UN_AUTHORIZED, "未登陆用户");
    public final RuntimeException INVALID_EXCHANGE_CODE = new ServiceException("无效兑换码，请前往`立即充值`进行购买");
    public final RuntimeException CURRENT_EXCHANGE_NOT_EXIST = new ServiceException("当前兑换码不存在");
    public final RuntimeException CURRENT_EXCHANGE_IS_USED = new ServiceException("当前兑换码已使用");
    public final RuntimeException CURRENT_EXCHANGE_EXPIRED = new ServiceException("当前兑换码已过期");
    public final RuntimeException CURRENT_EXCHANGE_DISABLED = new ServiceException("当前兑换码已禁用，请联系管理员");
    public final RuntimeException CURRENT_USER_NOT_EXIST = new ServiceException("当前用户不存在");
    public final RuntimeException CHAT_USER_USAGE_INCREASE_ERROR = new ServiceException("兑换过于频繁，请稍后再试");


    public ServiceException of(final String message) {
        return new ServiceException(message);
    }

    public ServiceException of(final ResultCode code, final String message) {
        return new ServiceException(code, message);
    }
}

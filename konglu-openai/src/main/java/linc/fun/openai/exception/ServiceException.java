package linc.fun.openai.exception;

import linc.fun.openai.domain.vo.response.IResultCode;
import linc.fun.openai.domain.vo.response.ResultCode;
import lombok.Getter;

/**
 * @author linc
 * @date 2023-3-23
 * 业务异常
 */
public class ServiceException extends RuntimeException {

    @Getter
    private final IResultCode resultCode;

    public ServiceException(String message) {
        super(message);
        this.resultCode = ResultCode.FAILURE;
    }

    public ServiceException(ResultCode code, String message) {
        super(message);
        this.resultCode = code;
    }

    public ServiceException(final IResultCode resultCode, String message) {
        super(message);
        this.resultCode = resultCode;
    }
}

package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.EnableDisableStatusEnum;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


/**
 * @author yqlin
 * @date 2023/5/8 00:02
 * @description chat商品表
 */

@Schema(description = "chat商品表")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_product")
public class ChatProductDO extends BaseDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 名称
     */
    @Column(value = "name")
    @Schema(description = "名称")
    private String name;
    /**
     * 奖励次数
     */
    @Column(value = "reward_use_times")
    @Schema(description = "奖励次数")
    private Integer rewardUseTimes;
    /**
     * 价格
     */
    @Column(value = "price")
    @Schema(description = "价格")
    private BigDecimal price;

    /**
     * 描述
     */
    @Column(value = "description")
    @Schema(description = "描述")
    private String description;

    /**
     * 销量
     */
    @Column(value = "sale")
    @Schema(description = "销量")
    private Integer sale;

    /**
     * 库存
     */
    @Column(value = "stock")
    @Schema(description = "库存")
    private Integer stock;

    /**
     * 状态 1:启用 2:停用
     */
    @Column(value = "status")
    @Schema(description = "状态 1:启用 2:停用")
    private EnableDisableStatusEnum status;

    /**
     * 是否删除 0 否 1 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    @Schema(description = "是否删除 0 否 1 是")
    private LogicDeleteEnum isDeleted;
}

package linc.fun.openai.domain.dto.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqlin
 * @date 2023/5/19 16:18
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MyExchangesPageQuery extends PageQuery {
    private String code;
}

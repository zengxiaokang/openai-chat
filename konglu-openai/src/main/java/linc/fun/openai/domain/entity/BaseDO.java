package linc.fun.openai.domain.entity;

import com.mybatisflex.annotation.Column;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author yqlin
 * @date 2023/5/4 18:17
 * @description
 */
@Data
public class BaseDO implements Serializable {

    /**
     * 创建时间
     */
    @Column(value = "create_time", onInsertValue = "now()")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @Column(value = "update_time", onInsertValue = "now()", onUpdateValue = "now()")
    private LocalDateTime updateTime;
}

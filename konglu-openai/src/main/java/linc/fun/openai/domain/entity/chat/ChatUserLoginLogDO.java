package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 前端用户登录日志表实体类
 */
@Table(value = "chat_user_login_log")
@Data
public class ChatUserLoginLogDO implements Serializable {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 登录的基础用户ID#chat_user
     */
    @Column(value = "user_id")
    private Long userId;

    /**
     * 登录方式（注册方式），邮箱登录，手机登录等等
     */
    @Column(value = "login_type")
    private ChatUserRegisterTypeEnum loginType;

    /**
     * 登录信息ID与login_type有关联，邮箱登录时关联#chat_user_binding_email
     */
    @Column(value = "user_binding_email_id")
    private Long userBindingEmailId;

    /**
     * 登录的IP地址
     */
    @Column(value = "login_ip")
    private String loginIp;

    /**
     * 登录状态，1登录成功，0登录失败
     */
    @Column(value = "login_status")
    private Boolean loginStatus;

    /**
     * 登录后返回的消息
     */
    @Column(value = "message")
    private String message;

    /**
     * 创建时间
     */
    @Column(value = "create_time", onInsertValue = "now()")
    private LocalDateTime createTime;
}

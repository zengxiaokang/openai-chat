package linc.fun.openai.domain.entity.sys;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.SysEmailBizTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 邮箱验证码核销记录表实体类
 * 记录某个邮箱发送了什么验证码，方便验证
 *
 * @author linc
 */
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_email_verify_code")
@Data
public class SysEmailVerifyCodeDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 验证码接收邮箱地址
     */
    @Column(value = "to_email_address")
    private String toEmailAddress;

    /**
     * 验证码唯一
     */
    @Column(value = "verify_code")
    private String verifyCode;

    /**
     * 是否使用，false 否，true 是
     */
    @Column(value = "is_used")
    private Boolean isUsed;

    /**
     * 核销IP，方便识别一些机器人账号
     */
    @Column(value = "verify_ip")
    private String verifyIp;

    /**
     * 验证码过期时间
     */
    @Column(value = "expire_at")
    private LocalDateTime expireAt;

    /**
     * 当前邮箱业务
     */
    @Column(value = "biz_type")
    private SysEmailBizTypeEnum bizType;

}

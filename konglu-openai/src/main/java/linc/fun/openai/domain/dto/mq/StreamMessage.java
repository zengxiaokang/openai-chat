package linc.fun.openai.domain.dto.mq;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author yqlin
 * @date 2023/5/10 23:19
 * @description
 */
@Data
@Accessors(chain = true)
public class StreamMessage {
    private String messageId;
    private String messageText;
}

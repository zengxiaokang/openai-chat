package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import lombok.*;

/**
 * 前端用户 基础表实体类
 *
 * @author CoDeleven
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(value = "chat_user")
@Data
public class ChatUserDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 昵称
     */
    @Column(value = "nickname")
    private String nickname;

    /**
     * 用户描述
     */
    @Column(value = "description")
    private String description;

    /**
     * 头像版本
     */
    @Column(value = "avatar_version")
    private Integer avatarVersion;

    /**
     * 上一次登录ip
     */
    @Column(value = "last_login_ip")
    private String lastLoginIp;

    /**
     * 可聊天次数
     */
    @Column(value = "can_conversation_times")
    private Integer canConversationTimes;


}

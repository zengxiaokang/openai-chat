package linc.fun.openai.domain.entity.chat;


import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description chat兑换记录表
 */
@Schema(description = "chat兑换记录表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_user_exchange_record")
public class ChatUserExchangeRecordDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 用户id#chat_user
     */
    @Column(value = "user_id")
    @Schema(description = "用户id#chat_user")
    private Long userId;

    /**
     * 兑换id#chat_exchange
     */
    @Column(value = "exchange_id")
    @Schema(description = "兑换id#chat_exchange")
    private Long exchangeId;

    /**
     * 通兑码#chat_exchange
     */
    @Column(value = "code")
    @Schema(description = "通兑码#chat_exchange")
    private String code;

    /**
     * 创建时间
     */
    @Column(value = "create_time", onInsertValue = "now()")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
}

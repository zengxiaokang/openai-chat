package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.EnableDisableStatusEnum;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author linc
 * @date 2023-3-28
 * 敏感词表实体类
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table("chat_sensitive_word")
public class ChatSensitiveWordDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 敏感词内容
     */
    @Column(value = "word")
    private String word;

    /**
     * 状态 1 启用 2 停用
     */
    @Column(value = "status")
    private EnableDisableStatusEnum status;

    /**
     * 是否删除 0 否 NULL 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    private LogicDeleteEnum isDeleted;
}

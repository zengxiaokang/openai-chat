package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import lombok.*;

/**
 * 前端用户邮箱扩展表实体类
 */
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(value = "chat_user_binding_email")
@Data
public class ChatUserBindingEmailDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 邮箱账号
     */
    @Column(value = "username")
    private String username;

    /**
     * 加密后的密码
     */
    @Column(value = "password")
    private String password;


    /**
     * 是否验证过，false 否 true 是
     */
    @Column(value = "verified")
    private Boolean verified;

}

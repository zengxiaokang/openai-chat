package linc.fun.openai.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yqlin
 * @date 2023/5/16 22:23
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyOrderVO {
    private Long orderId;
    private Long userId;
    private BigDecimal payAmount;
    private BigDecimal totalAmount;
    private Integer orderStatus;
    private LocalDateTime createTime;
    private List<MyOrderItemVO> myOrderItems;
}

package linc.fun.openai.domain.dto.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yqlin
 * @date 2023/5/17 01:15
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MyOrdersPageQuery extends PageQuery {
    private Long userId;
}

package linc.fun.openai.domain.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author yqlin
 * @date 2023/5/7 01:48
 * @description
 */
@Data
public class AlipayTradePreCreateResponse {
    @JSONField(name = "success")
    private Boolean success;
    @JSONField(name = "err_code")
    private Integer errCode;
    @JSONField(name = "err_message")
    private String errMessage;
    @JSONField(name = "data")
    private Data data;

    @lombok.Data
    public static class Data {
        @JSONField(name = "order_no")
        private String orderNo;
        @JSONField(name = "qr_code")
        private String qrCode;
    }
}

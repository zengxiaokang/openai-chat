package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description chat订单明细
 */

@Schema(description = "chat订单明细")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_order_item")
public class ChatOrderItemDO extends BaseDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 订单id#chat_order
     */
    @Column(value = "order_id")
    @Schema(description = "订单id#chat_order")
    private Long orderId;

    /**
     * 商品id#chat_product
     */
    @Column(value = "product_id")
    @Schema(description = "商品id#chat_product")
    private Long productId;

    /**
     * 兑换码数量
     */
    @Column(value = "exchange_num")
    @Schema(description = "兑换码数量")
    private Integer exchangeNum;

    /**
     * 商品名称#chat_product
     */
    @Column(value = "product_name")
    @Schema(description = "商品名称#chat_product")
    private String productName;

    /**
     * 商品价格#chat_product
     */
    @Column(value = "product_price")
    @Schema(description = "商品价格#chat_product")
    private BigDecimal productPrice;

    /**
     * 是否删除 0 否 1 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    @Schema(description = "是否删除 0 否 1 是")
    private LogicDeleteEnum isDeleted;
}

package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.ApiTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author linc
 * @date 2023-3-25
 * 聊天室表实体类
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table("chat_room")
public class ChatRoomDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 用户 id
     */
    @Column(value = "user_id")
    private Long userId;

    /**
     * 对话 id
     * 唯一
     */
    @Column(value = "conversation_id")
    private String conversationId;

    /**
     * ip
     */
    @Column(value = "ip")
    private String ip;

    /**
     * 第一条消息主键
     * 唯一
     */
    @Column(value = "first_chat_message_id")
    private Long firstChatMessageId;

    /**
     * 第一条消息 id
     * 唯一
     */
    @Column(value = "first_message_id")
    private String firstMessageId;

    /**
     * 对话标题
     */
    @Column(value = "title")
    private String title;

    /**
     * API 类型
     * 不同类型的对话不能一起存储
     */
    @Column(value = "api_type")
    private ApiTypeEnum apiType;
}

package linc.fun.openai.domain.bo;

import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import lombok.Data;

/**
 * @author linc
 * @date 2023-4-16
 * JWT 用户信息业务参数
 */
@Data
public class JwtUserInfoBO {

    /**
     * 注册类型
     */
    private ChatUserRegisterTypeEnum registerType;

    /**
     * 登录账号
     */
    private String username;

    /**
     * 基础用户 id
     */
    private Long userId;

    /**
     * 扩展用户 id
     */
    private Long userBindingEmailId;
}

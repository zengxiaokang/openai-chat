package linc.fun.openai.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MyOrderItemVO {
    private Long orderId;
    private Long orderItemId;
    private Long productId;
    private Integer exchangeNum;
    private String productName;
    private BigDecimal productPrice;
}

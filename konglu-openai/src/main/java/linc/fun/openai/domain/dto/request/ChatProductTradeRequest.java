package linc.fun.openai.domain.dto.request;

import cn.hutool.core.collection.CollUtil;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yqlin
 * @date 2023/5/8 20:15
 * @description
 */
@Data
public class ChatProductTradeRequest {
    @Valid
    @NotEmpty(message = "商品不能为空")
    @Size(min = 1, max = 4, message = "选择商品只能1到4个")
    private List<ProductTradeItem> selectedProductItems;

    public List<Long> getSelectedProductIds() {
        if (CollUtil.isEmpty(selectedProductItems)) return Collections.emptyList();
        return selectedProductItems.stream().map(ProductTradeItem::getProductId).toList();
    }

    public Map<Long, Integer> getSelectedProductMap() {
        if (CollUtil.isEmpty(selectedProductItems)) return new HashMap<>();
        return selectedProductItems.stream().collect(Collectors.toMap(ProductTradeItem::getProductId, ProductTradeItem::getNum));
    }
}

package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.ChatOrderStatusEnum;
import linc.fun.openai.enums.ChatOrderTypeEnum;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description chat订单信息表
 */
@Schema(description = "chat订单信息表")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_order")
public class ChatOrderDO extends BaseDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 用户id#chat_user
     */
    @Column(value = "user_id")
    @Schema(description = "用户id#chat_user")
    private Long userId;

    /**
     * 订单总金额
     */
    @Column(value = "total_amount")
    @Schema(description = "订单总金额")
    private BigDecimal totalAmount;

    /**
     * 应付金额
     */
    @Column(value = "pay_amount")
    @Schema(description = "应付金额")
    private BigDecimal payAmount;

    /**
     * 订单状态(0:待付款 1:待确认 2:已完成 3:无效订单 4已关闭
     */
    @Column(value = "status")
    @Schema(description = "订单状态(0:待付款 1:待确认 2:已完成 3:无效订单 4已关闭")
    private ChatOrderStatusEnum status;

    /**
     * 订单类型(0:正常订单 1:秒杀订单)
     */
    @Column(value = "type")
    @Schema(description = "订单类型(0:正常订单 1:秒杀订单)")
    private ChatOrderTypeEnum type;

    /**
     * 备注
     */
    @Column(value = "remark")
    @Schema(description = "备注")
    private String remark;

    /**
     * 支付时间
     */
    @Column(value = "pay_time")
    @Schema(description = "支付时间")
    private LocalDateTime payTime;

    /**
     * 是否删除 0 否 1 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    @Schema(description = "是否删除 0 否 1 是")
    private LogicDeleteEnum isDeleted;
}

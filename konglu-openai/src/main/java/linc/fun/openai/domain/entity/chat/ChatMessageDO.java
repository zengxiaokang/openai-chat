package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.ApiTypeEnum;
import linc.fun.openai.enums.ChatMessageStatusEnum;
import linc.fun.openai.enums.ChatMessageTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author linc
 * @date 2023-3-25
 * 聊天记录表实体类
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table("chat_message")
public class ChatMessageDO extends BaseDO {

    /**
     * 主键
     */
    @Id(value = "id", keyType = KeyType.None)
    private Long id;

    /**
     * 用户 id
     */
    @Column(value = "user_id")
    private Long userId;

    /**
     * 消息 id
     */
    @Column(value = "message_id")
    private String messageId;

    /**
     * 父级消息 id
     * 第一条消息父级消息 id 为空
     * 回答的父级消息 id 不能为空
     */
    @Column(value = "parent_message_id")
    private String parentMessageId;

    /**
     * 父级回答消息 id
     * 当前消息是问题：则 parentMessageId=parentAnswerMessageId
     */
    @Column(value = "parent_answer_message_id")
    private String parentAnswerMessageId;

    /**
     * 父级问题消息 id
     * 当前消息是回答：则 parentMessageId=parentQuestionMessageId
     */
    @Column(value = "parent_question_message_id")
    private String parentQuestionMessageId;

    /**
     * 上下文数量
     * 包含了问题和回答的数量
     * 第一条消息是 1
     */
    @Column(value = "context_count")
    private Integer contextCount;

    /**
     * 问题上下文数量
     * 包含了连续的问题的数量
     * 第一条消息是 1
     */
    @Column(value = "question_context_count")
    private Integer questionContextCount;

    /**
     * 消息类型枚举
     * 第一条消息一定是问题
     */
    @Column(value = "message_type")
    private ChatMessageTypeEnum messageType;

    /**
     * 聊天室 id
     */
    @Column(value = "chat_room_id")
    private Long chatRoomId;

    /**
     * 对话 id
     */
    @Column(value = "conversation_id")
    private String conversationId;

    /**
     * API 类型
     */
    @Column(value = "api_type")
    private ApiTypeEnum apiType;

    /**
     * 模型名称
     */
    @Column(value = "model_name")
    private String modelName;

    /**
     * ip
     */
    @Column(value = "ip")
    private String ip;

    /**
     * apiKey
     */
    @Column(value = "api_key")
    private String apiKey;

    /**
     * 消息内容
     * 包含上下文的对话这里只会显示出用户发送的
     */
    @Column(value = "content")
    private String content;

    /**
     * 消息的原始数据
     * 问题：请求参数
     * 回答：响应参数
     */
    @Column(value = "original_data")
    private String originalData;

    /**
     * 错误响应数据
     */
    @Column(value = "response_error_data")
    private String responseErrorData;

    /**
     * 输入消息的 tokens
     */
    @Column(value = "prompt_tokens")
    private Integer promptTokens;

    /**
     * 输出消息的 tokens
     */
    @Column(value = "completion_tokens")
    private Integer completionTokens;

    /**
     * 累计 Tokens
     */
    @Column(value = "total_tokens")
    private Integer totalTokens;

    /**
     * 聊天信息状态
     */
    @Column(value = "status")
    private ChatMessageStatusEnum status;

    /**
     * 是否被隐藏
     */
    @Column(value = "is_hide")
    private Boolean isHide;

}

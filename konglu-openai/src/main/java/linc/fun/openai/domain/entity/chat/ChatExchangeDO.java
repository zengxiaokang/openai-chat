package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.EnableDisableStatusEnum;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description chat兑换表
 */
@Schema(description = "chat兑换表")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_exchange")
public class ChatExchangeDO extends BaseDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 商品id#chat_product
     */
    @Column(value = "product_id")
    @Schema(description = "商品id#chat_product")
    private Long productId;

    /**
     * 名称
     */
    @Column(value = "name")
    @Schema(description = "名称")
    private String name;

    /**
     * 通兑码
     */
    @Column(value = "code")
    @Schema(description = "通兑码")
    private String code;

    /**
     * 奖励次数
     */
    @Column(value = "reward_use_times")
    @Schema(description = "奖励次数")
    private Integer rewardUseTimes;

    /**
     * 面值
     */
    @Column(value = "price")
    @Schema(description = "面值")
    private BigDecimal price;

    /**
     * 是否使用，false 否，true 是
     */
    @Column(value = "is_used")
    @Schema(description = "是否使用，false 否，true 是")
    private Boolean isUsed;

    /**
     * 状态 1:启用 2:停用
     */
    @Column(value = "status")
    @Schema(description = "状态 1:启用 2:停用")
    private EnableDisableStatusEnum status;

    /**
     * 是否删除 0 否 1 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    @Schema(description = "是否删除 0 否 1 是")
    private LogicDeleteEnum isDeleted;

    /**
     * 多少天后过期，默认90天
     */
    @Column(value = "expires_days")
    @Schema(description = "多少天后过期，默认90天")
    private Integer expiresDays;
    /**
     * 激活时间
     */
    @Column(value = "activation_time")
    @Schema(description = "激活时间")
    private LocalDateTime activationTime;
}

package linc.fun.openai.domain.entity.sys;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.SysEmailBizTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 邮箱发送日志表实体类
 * 用于审计日志
 */
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_email_send_log")
@Data
public class SysEmailSendLogDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 发件人邮箱
     */
    @Column(value = "from_email_address")
    private String fromEmailAddress;

    /**
     * 收件人邮箱
     */
    @Column(value = "to_email_address")
    private String toEmailAddress;

    /**
     * 业务类型
     */
    @Column(value = "biz_type")
    private SysEmailBizTypeEnum bizType;

    /**
     * 请求 ip
     */
    @Column(value = "request_ip")
    private String requestIp;

    /**
     * 发送内容
     */
    @Column(value = "content")
    private String content;

    /**
     * 发送状态，1成功，0失败
     */
    @Column(value = "status")
    private Integer status;

    /**
     * 消息id
     */
    @Column(value = "message_id")
    private String messageId;

    /**
     * 发送后的消息，用于记录成功/失败的信息，成功默认为 success
     */
    @Column(value = "message")
    private String message;
}

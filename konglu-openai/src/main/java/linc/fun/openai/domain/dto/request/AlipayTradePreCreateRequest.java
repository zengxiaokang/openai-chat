package linc.fun.openai.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author yqlin
 * @date 2023/5/7 01:21
 * @description
 */
@Data
public class AlipayTradePreCreateRequest {
    private String subject;

    @JsonProperty("total_amount")
    private String totalAmount;

    @JsonProperty("out_trade_no")
    private String outTradeNo;

    @JsonProperty("timeout_express")
    private String timeoutExpress;
    private String signature;
}

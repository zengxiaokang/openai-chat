package linc.fun.openai.domain.dto.request;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import linc.fun.openai.enums.ChatUserRegisterTypeEnum;
import linc.fun.openai.handler.validation.annotation.ChatUserRegisterAvailable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 前端用户注册请求
 *
 * @author CoDeleven
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ChatUserRegisterAvailable
@Schema(title = "前端用户注册请求，适用于邮箱登录登录")
public class RegisterFrontUserForEmailRequest {

    @Size(min = 6, max = 64, message = "用户名长度应为6~64个字符")
    @NotNull
    @Schema(title = "用户ID，可以为邮箱，可以为手机号")
    private String identity;

    /**
     * TODO 正则校验
     */
    @Schema(title = "密码")
    @NotNull(message = "密码不能为空")
    private String password;


    private ChatUserRegisterTypeEnum registerType = ChatUserRegisterTypeEnum.EMAIL;
}

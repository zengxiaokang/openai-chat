package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.ChatOrderPayStatusEnum;
import linc.fun.openai.enums.ChatOrderStatusEnum;
import linc.fun.openai.enums.LogicDeleteEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description chat订单历史记录表
 */
@Schema(description = "chat订单历史记录表")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_order_history")
public class ChatOrderHistoryDO extends BaseDO {
    /**
     * ID
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "ID")
    private Long id;

    /**
     * 用户id#chat_user
     */
    @Column(value = "user_id")
    @Schema(description = "用户id#chat_user")
    private Long userId;

    /**
     * 订单id#chat_order
     */
    @Column(value = "order_id")
    @Schema(description = "订单id#chat_order")
    private Long orderId;

    /**
     * 订单状态 0:待付款 1:待确认 2:已完成 3:无效订单 4:已关闭 #chat_order
     */
    @Column(value = "order_status")
    @Schema(description = "订单状态 0:待付款 1:待确认 2:已完成 3:无效订单 4:已关闭 #chat_order")
    private ChatOrderStatusEnum orderStatus;

    /**
     * 支付状态 0:支付中(0:待付款 1:待确认) 1:支付成功(2:已完成)  2:支付失败(3:无效订单)
     */
    @Column(value = "pay_status")
    @Schema(description = "支付状态 0:支付中(0:待付款 1:待确认) 1:支付成功(2:已完成)  2:支付失败(3:无效订单)")
    private ChatOrderPayStatusEnum payStatus;

    /**
     * 支付二维码链接
     */
    @Column(value = "pay_qr_code")
    @Schema(description = "支付二维码链接")
    private String payQrCode;

    /**
     * 订单总金额
     */
    @Column(value = "total_amount")
    @Schema(description = "订单总金额")
    private BigDecimal totalAmount;


    /**
     * 应付金额
     */
    @Column(value = "pay_amount")
    @Schema(description = "应付金额")
    private BigDecimal payAmount;

    /**
     * 备注
     */
    @Column(value = "remark")
    @Schema(description = "备注")
    private String remark;

    /**
     * 支付账户
     */
    @Column(value = "pay_account")
    @Schema(description = "支付账户")
    private String payAccount;

    /**
     * 是否删除 0 否 1 是
     */
    @Column(value = "is_deleted", isLogicDelete = true)
    @Schema(description = "是否删除 0 否 1 是")
    private LogicDeleteEnum isDeleted;
}

package linc.fun.openai.domain.dto.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author yqlin
 * @date 2023/5/5 22:13
 * @description
 */
@Data
public class AlipayFace2faceTradeRequest {
    @NotNull(message = "订单号不能为空")
    private Long orderNo;
}

package linc.fun.openai.domain.vo;

import lombok.Data;

/**
 * @author yqlin
 * @date 2023/5/5 22:35
 * @description
 */
@Data
public class AlipayFace2faceTradeResponse {
    private String orderNo;
    private String qrCode;
    private String timeoutExpress;
}

package linc.fun.openai.domain.vo;

import linc.fun.openai.domain.entity.chat.ChatProductDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author yqlin
 * @date 2023/5/15 20:45
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatTop4ProductsVO {
    private List<ChatProductDO> products;
}


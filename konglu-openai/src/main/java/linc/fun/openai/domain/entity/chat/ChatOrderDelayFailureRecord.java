package linc.fun.openai.domain.entity.chat;


import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


/**
 * @author yqlin
 * @date 2023/5/11 18:42
 * @description 超时订单失败记录表
 */

@Schema(description = "超时订单失败记录表")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(value = "chat_order_delay_failure_record")
public class ChatOrderDelayFailureRecord {
    /**
     * 主键
     */
    @Id(value = "id", keyType = KeyType.None)
    @Schema(description = "主键")
    private Long id;

    /**
     * 错误信息
     */
    @Column(value = "error_message")
    @Schema(description = "错误信息")
    private String errorMessage;

    /**
     * 消息内容 json
     */
    @Column(value = "message_payload")
    @Schema(description = "消息内容 json")
    private String messagePayload;

    /**
     * 重试次数
     */
    @Column(value = "retry_count")
    @Schema(description = "重试次数")
    private Integer retryCount;

    /**
     * 创建时间
     */
    @Column(value = "create_time", onInsertValue = "now()")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
}

package linc.fun.openai.domain.dto.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ProductTradeItem {
    @NotNull(message = "商品ID不能为空")
    private Long productId;

    @NotNull(message = "商品数量不能为为空")
    @Min(value = 1, message = "每个商品数量最小为1")
    @Max(value = 10, message = "每个商品数量最大为10")
    private Integer num;
}

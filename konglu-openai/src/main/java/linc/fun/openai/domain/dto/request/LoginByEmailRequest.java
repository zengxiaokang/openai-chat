package linc.fun.openai.domain.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 前端用户登录请求
 *
 * @author CoDeleven
 */
@Data
@Schema(title = "前端用户登录请求")
public class LoginByEmailRequest {

    @Schema(title = "邮箱地址")
    @NotBlank(message = "邮箱地址不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    @Schema(title = "密码")
    private String password;
}

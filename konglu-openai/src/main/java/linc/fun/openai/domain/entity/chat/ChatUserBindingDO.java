package linc.fun.openai.domain.entity.chat;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import linc.fun.openai.domain.entity.BaseDO;
import linc.fun.openai.enums.ChatUserBindingTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 前端用户绑定表实体类
 * 记录了 基础用户 和 登录方式 的绑定关系
 */
@EqualsAndHashCode(callSuper = true)
@Table(value = "chat_user_binding")
@Data
public class ChatUserBindingDO extends BaseDO {

    /**
     * 主键
     */
    @Id(keyType = KeyType.None)
    private Long id;

    /**
     * 绑定类型
     */
    @Column(value = "binding_type")
    private ChatUserBindingTypeEnum bindingType;

    /**
     * 额外信息表的用户ID
     */
    @Column(value = "user_binding_email_id")
    private Long userBindingEmailId;

    /**
     * 基础用户表的ID
     */
    @Column(value = "user_id")
    private Long userId;
}

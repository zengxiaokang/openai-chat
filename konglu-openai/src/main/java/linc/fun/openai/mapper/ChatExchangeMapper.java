package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatExchangeDO;


/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description
 */
public interface ChatExchangeMapper extends BaseMapper<ChatExchangeDO> {
}

package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatUserExchangeRecordDO;


/**
 * @author yqlin
 * @date 2023/5/8 00:04
 * @description
 */
public interface ChatUserExchangeRecordMapper extends BaseMapper<ChatUserExchangeRecordDO> {
}

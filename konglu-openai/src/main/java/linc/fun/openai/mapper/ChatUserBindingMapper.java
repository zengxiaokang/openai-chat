package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatUserBindingDO;

/**
 * @author codeleven
 * 前端用户绑定数据访问层
 */
public interface ChatUserBindingMapper extends BaseMapper<ChatUserBindingDO> {

}

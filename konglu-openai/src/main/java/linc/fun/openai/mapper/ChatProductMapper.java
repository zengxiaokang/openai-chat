package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatProductDO;


/**
 * @author yqlin
 * @date 2023/5/8 00:02
 * @description
 */
public interface ChatProductMapper extends BaseMapper<ChatProductDO> {
}

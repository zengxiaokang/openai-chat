package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatOrderDelayFailureRecord;


/**
 * @author yqlin
 * @date 2023/5/11 18:42
 * @description
 */
public interface ChatOrderDelayFailureRecordMapper extends BaseMapper<ChatOrderDelayFailureRecord> {
}

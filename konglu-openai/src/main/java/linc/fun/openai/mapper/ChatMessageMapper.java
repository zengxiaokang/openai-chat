package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatMessageDO;

/**
 * @author linc
 * @date 2023-4-22
 * 聊天记录数据访问层
 */
public interface ChatMessageMapper extends BaseMapper<ChatMessageDO> {
}

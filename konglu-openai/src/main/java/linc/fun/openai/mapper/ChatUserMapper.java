package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatUserDO;

/**
 * 前端基础用户数据访问层
 *
 * @author CoDeleven
 * @date 2023.4.8
 */
public interface ChatUserMapper extends BaseMapper<ChatUserDO> {
}

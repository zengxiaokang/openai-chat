package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatOrderItemDO;


/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
public interface ChatOrderItemMapper extends BaseMapper<ChatOrderItemDO> {
}

package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatUserLoginLogDO;

/**
 * 前端用户登录日志数据访问层
 *
 * @author CoDeleven
 */
public interface ChatUserLoginLogMapper extends BaseMapper<ChatUserLoginLogDO> {

}

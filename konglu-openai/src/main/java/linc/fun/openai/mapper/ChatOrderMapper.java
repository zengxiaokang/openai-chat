package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.dto.query.MyOrdersPageQuery;
import linc.fun.openai.domain.entity.chat.ChatOrderDO;
import linc.fun.openai.domain.vo.MyOrderVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @author yqlin
 * @date 2023/5/8 00:05
 * @description
 */
public interface ChatOrderMapper extends BaseMapper<ChatOrderDO> {
    List<MyOrderVO> queryMyOrdersByUserId(@Param("query") MyOrdersPageQuery query);

    long queryMyOrderTotal(@Param("query")  MyOrdersPageQuery query);
}

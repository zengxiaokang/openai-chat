package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatSensitiveWordDO;

/**
 * @author linc
 * @date 2023-3-28
 * 敏感词数据库访问层
 */
public interface ChatSensitiveWordMapper extends BaseMapper<ChatSensitiveWordDO> {

}

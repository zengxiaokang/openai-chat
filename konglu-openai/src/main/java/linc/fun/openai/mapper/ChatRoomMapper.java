package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatRoomDO;

/**
 * @author linc
 * @date 2023-4-22
 * 聊天室数据访问层
 */
public interface ChatRoomMapper extends BaseMapper<ChatRoomDO> {
}

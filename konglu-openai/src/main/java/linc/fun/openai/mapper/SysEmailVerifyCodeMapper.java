package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.sys.SysEmailVerifyCodeDO;

/**
 * 邮箱验证码核销记录数据访问层
 *
 * @author CoDeleven
 */
public interface SysEmailVerifyCodeMapper extends BaseMapper<SysEmailVerifyCodeDO> {

}

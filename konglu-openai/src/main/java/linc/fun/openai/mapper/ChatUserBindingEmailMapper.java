package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.chat.ChatUserBindingEmailDO;

/**
 * @author CoDeleven
 * 前端用户邮箱登录数据访问层
 */
public interface ChatUserBindingEmailMapper extends BaseMapper<ChatUserBindingEmailDO> {

}

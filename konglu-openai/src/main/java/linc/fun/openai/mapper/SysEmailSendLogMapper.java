package linc.fun.openai.mapper;

import com.mybatisflex.core.BaseMapper;
import linc.fun.openai.domain.entity.sys.SysEmailSendLogDO;

/**
 * 邮箱发送日志数据访问层
 *
 * @author CoDeleven
 */
public interface SysEmailSendLogMapper extends BaseMapper<SysEmailSendLogDO> {

}

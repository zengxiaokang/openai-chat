package linc.fun.openai.config.code;

import linc.fun.openai.factory.CaptchaFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yqlin
 * @date 2023/5/4 21:56
 * @description
 */
@Configuration
public class CaptchaConfig {

    /**
     * 验证码工厂配置
     */
    @Bean
    @ConfigurationProperties(prefix = "captcha")
    public CaptchaFactory captchaFactory() {
        return new CaptchaFactory();
    }

}

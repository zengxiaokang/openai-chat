package linc.fun.openai.config.exchange;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yqlin
 * @date 2023/5/12 13:15
 * @description
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "exchange")
public class ExchangeConfig {
    private String secret;
    private Integer groupIdStart;
    private Integer groupIdEnd;
    private Integer codeLength;
}

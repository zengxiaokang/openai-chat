package linc.fun.openai.config.mflex;

import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * @author yqlin
 * @date 2023/5/13 16:26
 * @description
 */
@Slf4j
@Configuration
public class MybatisFlexConfig {
    public MybatisFlexConfig() {

        FlexGlobalConfig globalConfig = FlexGlobalConfig.getDefaultConfig();
        //设置数据库正常时的值
        globalConfig.setNormalValueOfLogicDelete(0);
        //设置数据已被删除时的值
        globalConfig.setDeletedValueOfLogicDelete(1);

        //开启审计功能
        AuditManager.setAuditEnable(true);
        //设置 SQL 审计收集器
        AuditManager.setMessageCollector(auditMessage -> {
            log.info("执行sql:\n {}\n耗时: {}ms",
                    auditMessage.getFullSql(),
                    auditMessage.getElapsedTime());
        });
    }
}

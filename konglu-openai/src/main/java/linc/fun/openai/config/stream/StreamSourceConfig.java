package linc.fun.openai.config.stream;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * @author yqlin
 * @date 2023/5/11 00:27
 * @description
 */
@Slf4j
@Configuration
public class StreamSourceConfig {
    /**
     * MQ 发送助手
     * 通过 AtomicReference 将 FluxSink 暴露出去. 通过 FluxSink.next 发送消息
     *
     * @return MQ
     */
    @Bean
    public AtomicReference<FluxSink<Message<String>>> mqSender() {
        return new AtomicReference<>();
    }

    @Bean("chat-order-source")
    public Supplier<Flux<Message<String>>> chatOrderSource(AtomicReference<FluxSink<Message<String>>> mqSender) {
        return () -> Flux.create(mqSender::set);
    }
}

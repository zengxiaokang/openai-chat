package linc.fun.openai.config.openai;

import cn.hutool.core.lang.Opt;
import cn.hutool.core.util.StrUtil;
import com.unfbx.chatgpt.entity.chat.ChatCompletion;
import linc.fun.openai.enums.ApiTypeEnum;
import linc.fun.openai.enums.ChatConversationModelEnum;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * 聊天配置参数
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "openai")
public class OpenaiConfig implements InitializingBean {

    /**
     * OpenAI API Key
     *
     * @link <a href="https://beta.openai.com/docs/api-reference/authentication"/>
     */
    private String apiKey;

    /**
     * Access Token
     *
     * @link <a href="https://chat.openai.com/api/auth/session"/>
     */
    private String accessToken;

    /**
     * OpenAI API Base URL
     *
     * @link <a href="https://api.openai.com"/>
     */
    private String apiBaseUrl;

    /**
     * OpenAI API Model
     *
     * @link <a href="https://beta.openai.com/docs/models"/>
     */
    private String apiModel;

    /**
     * 反向代理地址，AccessToken 时用到
     */
    private String apiReverseProxy;

    /**
     * 超时毫秒
     */
    private Integer timeoutMs;

    /**
     * HTTP 代理主机
     */
    private String httpProxyHost;

    /**
     * HTTP 代理端口
     */
    private Integer httpProxyPort;

    /**
     * 管理端账号
     */
    private String adminAccount;

    /**
     * 管理端密码
     */
    private String adminPassword;

    /**
     * 全局时间内最大请求次数，默认无限制
     */
    private Integer maxRequest;

    /**
     * 全局最大请求时间间隔（秒）
     */
    private Integer maxRequestSecond;

    /**
     * ip 时间内最大请求次数，默认无限制
     */
    private Integer ipMaxRequest;

    /**
     * ip 最大请求时间间隔（秒）
     */
    private Integer ipMaxRequestSecond;

    /**
     * 限制上下文对话的问题数量，默认不限制
     */
    private Integer limitQuestionContextCount;

    /**
     * 判断是否有 http 代理
     *
     * @return true/false
     */
    public Boolean hasHttpProxy() {
        return StrUtil.isNotBlank(httpProxyHost) && Objects.nonNull(httpProxyPort);
    }

    /**
     * 获取 API 类型枚举
     *
     * @return API 类型枚举
     */
    public ApiTypeEnum getApiTypeEnum() {
        // 优先 API KEY
        if (StrUtil.isNotBlank(apiKey)) {
            return ApiTypeEnum.API_KEY;
        }
        return ApiTypeEnum.ACCESS_TOKEN;
    }

    /**
     * 获取全局时间内最大请求次数
     *
     * @return 最大请求次数
     */
    public Integer getMaxRequest() {
        return Opt.ofNullable(maxRequest).orElse(0);
    }

    /**
     * 获取全局最大请求时间间隔（秒）
     *
     * @return 时间间隔
     */
    public Integer getMaxRequestSecond() {
        return Opt.ofNullable(maxRequestSecond).orElse(0);
    }

    /**
     * 获取 ip 时间内最大请求次数
     *
     * @return 最大请求次数
     */
    public Integer getIpMaxRequest() {
        return Opt.ofNullable(ipMaxRequest).orElse(0);
    }

    /**
     * 获取 ip 最大请求时间间隔（秒）
     *
     * @return 时间间隔
     */
    public Integer getIpMaxRequestSecond() {
        return Opt.ofNullable(ipMaxRequestSecond).orElse(0);
    }

    /**
     * 获取限制的上下文对话数量
     *
     * @return 限制数量
     */
    public Integer getLimitQuestionContextCount() {
        return Opt.ofNullable(limitQuestionContextCount).orElse(0);
    }

    @Override
    public void afterPropertiesSet() {
        if (StrUtil.isBlank(apiKey) && StrUtil.isBlank(accessToken)) {
            throw new RuntimeException("apiKey 或 accessToken 必须有值");
        }

        // ApiKey
        if (getApiTypeEnum() == ApiTypeEnum.API_KEY) {
            // apiBaseUrl 必须有值
            if (StrUtil.isBlank(apiBaseUrl)) {
                throw new RuntimeException("openaiApiBaseUrl 必须有值");
            }

            // 校验 apiModel
            if (StrUtil.isBlank(apiModel)) {
                apiModel = ChatCompletion.Model.GPT_3_5_TURBO.getName();
                return;
            }
            ChatCompletion.Model[] models = ChatCompletion.Model.values();
            for (ChatCompletion.Model model : models) {
                if (model.getName().equals(apiModel)) {
                    return;
                }
            }
            throw new RuntimeException("ApiKey apiModel 填写错误");
        }

        // AccessToken
        if (getApiTypeEnum() == ApiTypeEnum.ACCESS_TOKEN) {
            // apiReverseProxy 必须有值
            if (StrUtil.isBlank(apiReverseProxy)) {
                throw new RuntimeException("apiReverseProxy 必须有值");
            }

            // 校验 apiModel
            if (StrUtil.isBlank(apiModel)) {
                apiModel = ChatConversationModelEnum.GPT_3_5_TURBO.getName();
                return;
            }

            if (!ChatConversationModelEnum.NAME_MAP.containsKey(apiModel)) {
                throw new RuntimeException("AccessToken apiModel 填写错误");
            }
        }
    }
}

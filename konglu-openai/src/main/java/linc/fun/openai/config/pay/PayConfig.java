package linc.fun.openai.config.pay;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yqlin
 * @date 2023/5/7 00:45
 * @description
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "pay")
public class PayConfig {
    private String alipayFace2faceUrl;
    private String signature;
    private String timeoutExpress;

}

package linc.fun.openai;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.crypto.BCUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.digest.MD5;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author yqlin
 * @date 2023/5/6 20:23
 * @description
 */
@Slf4j
@SpringBootTest
public class RsaTest {

    final String encode = "p5qaT+zwuIwP/suFTsmFLEykJJ22M23tXM2sWBR5xxzDneVaHWmRtG7IEmjytb9es1ER82X0ZkS/8XDEitQYFZo47OG29wYq/KwhqB18/778MU7CSczvEzChgDIK2qRENm6t4hxIh3H6N7xT/WpHfJc48K67LxP53oAZBah8elcXUyodbURHclbE1V0/YGc+GzZZtuiJxNd9tHG3rNqjFpNx+zelLNChZyvRbDkD89vM9/XCX5eLPtec3C//HgGVA2ClDgF8k1gQSCuNdQw+Nks3vSbaZg1ft4exrAVCQc1MbL2o8LPBhq93SmstyNxC83FntGb9wf4VMMe9m4ZWxg==";

    @Test
    void test() {
        try {
            ClassPathResource priPath = new ClassPathResource("rsa/private_key.pem");
            InputStream pris = priPath.getStream();
            ClassPathResource pubPath = new ClassPathResource("rsa/public_key.pem");
            InputStream pubis = pubPath.getStream();
            PublicKey pubKey = BCUtil.readPemPublicKey(pubis);
            PrivateKey priKey = BCUtil.readPemPrivateKey(pris);
            String publicKey = Base64Encoder.encode(pubKey.getEncoded());
            System.out.println(publicKey);
            String privateKey = Base64Encoder.encode(priKey.getEncoded());
            System.out.println(privateKey);
            String decoded = Base64.decodeStr(encode);
//            String text = "Hello, world!";
//            String encryptedText = encrypt(text, publicKey);
            String decryptedText = decrypt(decoded, privateKey);
//            System.out.println("Original text: " + text);
//            System.out.println("Encrypted text: " + encryptedText);
            System.out.println("Decrypted text: " + decryptedText);
        } catch (Exception e) {
            log.error("e", e);
        }
    }

    // 随机生成密钥对
    public static String[] generateKeyPair() {
        RSA rsa = new RSA();
        String publicKey = rsa.getPublicKeyBase64();
        String privateKey = rsa.getPrivateKeyBase64();
        return new String[]{publicKey, privateKey};
    }

    // RSA加密
    public static String encrypt(String text, String publicKey) {
        RSA rsa = new RSA(null, publicKey);
        byte[] encrypted = rsa.encrypt(text.getBytes(), KeyType.PublicKey);
        return Base64.encode(encrypted);
    }

    // RSA解密
    public static String decrypt(String text, String privateKey) {
        RSA rsa = new RSA(privateKey, null);
        byte[] decrypted = rsa.decrypt(Base64.decode(text), KeyType.PrivateKey);
        return new String(decrypted);
    }

    public static void main(String[] args) {
        String[] keyPair = generateKeyPair();
        String publicKey = keyPair[0];
        System.out.println(publicKey);
        String privateKey = keyPair[1];
        System.out.println(privateKey);
        String text = "Hello, world!";
        String encryptedText = encrypt(text, publicKey);
        String decryptedText = decrypt(encryptedText, privateKey);
        System.out.println("Original text: " + text);
        System.out.println("Encrypted text: " + encryptedText);
        System.out.println("Decrypted text: " + decryptedText);
        MD5 md5 = MD5.create();
        System.out.println(md5.digestHex("123456789"));
    }
}




SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

create database open_ai_chat charset utf8mb4;
use open_ai_chat;
-- ----------------------------
-- Table structure for chat_exchange
-- ----------------------------
DROP TABLE IF EXISTS chat_exchange;
CREATE TABLE chat_exchange
(
    id               bigint(20)     NOT NULL COMMENT 'ID',
    product_id       bigint(20)     NOT NULL COMMENT '商品id#chat_product',
    name             varchar(30)    NOT NULL COMMENT '名称',
    code             varchar(100)   NOT NULL COMMENT '通兑码',
    reward_use_times int(11)        NOT NULL DEFAULT '0' COMMENT '奖励次数',
    price            decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '面值',
    is_used          tinyint(1)     NOT NULL DEFAULT '0' COMMENT '是否使用，false 否，true 是',
    is_occupied      tinyint(1)     NOT NULL DEFAULT '0' COMMENT '是否被占用: 0否 1是',
    status           tinyint(1)     NOT NULL DEFAULT '0' COMMENT '状态 1:启用 2:停用',
    is_deleted       int(11)        NOT NULL DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    activation_time  datetime                DEFAULT NULL COMMENT '激活时间',
    expires_days     int(11)                 DEFAULT '0' COMMENT '多少天后过期',
    create_time      datetime       NOT NULL COMMENT '创建时间',
    update_time      datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY code_uk (code) USING BTREE COMMENT '兑换码uk'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat聊天兑换表';

-- ----------------------------
-- Table structure for chat_message
-- ----------------------------
DROP TABLE IF EXISTS chat_message;
CREATE TABLE chat_message
(
    id                         bigint(20)    NOT NULL COMMENT 'ID',
    user_id                    bigint(20)    NOT NULL COMMENT '用户 id#chat_user',
    message_id                 varchar(255)  NOT NULL COMMENT '消息id',
    parent_message_id          varchar(255)           DEFAULT NULL COMMENT '父级消息 id',
    parent_answer_message_id   varchar(255)           DEFAULT NULL COMMENT '父级回答消息 id',
    parent_question_message_id varchar(255)           DEFAULT NULL COMMENT '父级问题消息 id',
    context_count              bigint(20)    NOT NULL COMMENT '上下文数量',
    question_context_count     bigint(20)    NOT NULL COMMENT '问题上下文数量',
    message_type               int(11)       NOT NULL COMMENT '消息类型枚举',
    chat_room_id               bigint(20)    NOT NULL COMMENT '聊天室 id#chat_room',
    conversation_id            varchar(255)           DEFAULT NULL COMMENT '对话 id',
    api_type                   varchar(20)   NOT NULL COMMENT 'API 类型',
    model_name                 varchar(50)            DEFAULT NULL COMMENT '模型名称',
    api_key                    varchar(255)           DEFAULT NULL COMMENT 'ApiKey',
    content                    varchar(5000) NOT NULL COMMENT '消息内容',
    original_data              text COMMENT '消息的原始数据',
    response_error_data        text COMMENT '错误响应数据',
    prompt_tokens              bigint(20)             DEFAULT NULL COMMENT '输入消息的 tokens',
    completion_tokens          bigint(20)             DEFAULT NULL COMMENT '输出消息的 tokens',
    total_tokens               bigint(20)             DEFAULT NULL COMMENT '累计 Tokens',
    ip                         varchar(255)           DEFAULT NULL COMMENT 'ip',
    status                     int(11)       NOT NULL COMMENT '聊天信息状态',
    is_hide                    tinyint(1)    NOT NULL DEFAULT '0' COMMENT '是否被隐藏',
    create_time                datetime      NOT NULL COMMENT '创建时间',
    update_time                datetime      NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY message_id_uk (message_id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat聊天记录表';

-- ----------------------------
-- Table structure for chat_order
-- ----------------------------
DROP TABLE IF EXISTS chat_order;
CREATE TABLE chat_order
(
    id           bigint(20)     NOT NULL AUTO_INCREMENT COMMENT 'ID',
    user_id      bigint(20)     NOT NULL COMMENT '用户id#chat_user',
    total_amount decimal(10, 2) NOT NULL COMMENT '订单总金额',
    pay_amount   decimal(10, 2) NOT NULL COMMENT '应付金额',
    status       tinyint(4)     NOT NULL DEFAULT '0' COMMENT '订单状态(0:待付款 1:待确认 2:已完成 3:无效订单 4已关闭',
    type         tinyint(4)     NOT NULL DEFAULT '0' COMMENT '订单类型(0:正常订单 1:秒杀订单)',
    remark       varchar(500)            DEFAULT NULL COMMENT '备注',
    pay_time     datetime                DEFAULT NULL COMMENT '支付时间',
    is_deleted   int(11)        NOT NULL DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    create_time  datetime       NOT NULL COMMENT '创建时间',
    update_time  datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1662138650579697665
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat订单信息表';

-- ----------------------------
-- Table structure for chat_order_delay_failure_record
-- ----------------------------
DROP TABLE IF EXISTS chat_order_delay_failure_record;
CREATE TABLE chat_order_delay_failure_record
(
    id              bigint(20) NOT NULL COMMENT '主键',
    error_message   text       NOT NULL COMMENT '错误信息',
    message_payload longtext   NOT NULL COMMENT '消息内容 json',
    retry_count     int(11)    NOT NULL DEFAULT '0' COMMENT '重试次数',
    create_time     datetime   NOT NULL COMMENT '创建时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='Chat超时订单失败记录表';

-- ----------------------------
-- Table structure for chat_order_history
-- ----------------------------
DROP TABLE IF EXISTS chat_order_history;
CREATE TABLE chat_order_history
(
    id           bigint(20)     NOT NULL COMMENT 'ID',
    user_id      bigint(20)     NOT NULL COMMENT '用户id#chat_user',
    order_id     bigint(20)     NOT NULL COMMENT '订单id#chat_order',
    order_status tinyint(4)     NOT NULL DEFAULT '0' COMMENT '订单状态 0:待付款 1:待确认 2:已完成 3:无效订单 4:已关闭 #chat_order',
    pay_status   tinyint(4)     NOT NULL DEFAULT '0' COMMENT '支付状态 0:支付中(0:待付款 1:待确认) 1:支付成功(2:已完成)  2:支付失败(3:无效订单) 3:支付关闭\n',
    total_amount decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '订单总金额',
    pay_amount   decimal(10, 2) NOT NULL COMMENT '应付金额',
    pay_account  varchar(30)             DEFAULT NULL COMMENT '支付账户',
    pay_qr_code  varchar(255)            DEFAULT NULL COMMENT '支付二维码链接',
    is_deleted   int(11)        NOT NULL DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    create_time  datetime       NOT NULL COMMENT '创建时间',
    update_time  datetime       NOT NULL COMMENT '更新时间',
    remark       varchar(500)            DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat订单历史记录表';

-- ----------------------------
-- Table structure for chat_order_item
-- ----------------------------
DROP TABLE IF EXISTS chat_order_item;
CREATE TABLE chat_order_item
(
    id            bigint(20)     NOT NULL COMMENT 'ID',
    order_id      bigint(20)     NOT NULL COMMENT '订单id#chat_order',
    product_id    bigint(20)     NOT NULL COMMENT '商品id#chat_product',
    exchange_num  int(11)        NOT NULL COMMENT '兑换码数量',
    product_name  varchar(200)   NOT NULL COMMENT '商品名称#chat_product',
    product_price decimal(10, 2) NOT NULL COMMENT '商品价格#chat_product',
    is_deleted    int(11)        NOT NULL DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    create_time   datetime       NOT NULL COMMENT '创建时间',
    update_time   datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat订单明细表';

-- ----------------------------
-- Table structure for chat_product
-- ----------------------------
DROP TABLE IF EXISTS chat_product;
CREATE TABLE chat_product
(
    id               bigint(20)     NOT NULL COMMENT 'ID',
    name             varchar(30)    NOT NULL COMMENT '名称',
    reward_use_times int(11)        NOT NULL DEFAULT '0' COMMENT '奖励次数',
    price            decimal(10, 2) NOT NULL DEFAULT '0.00' COMMENT '价格',
    description      varchar(64)    NOT NULL COMMENT '描述',
    sale             int(11)        NOT NULL DEFAULT '0' COMMENT '销量',
    stock            int(11)        NOT NULL DEFAULT '0' COMMENT '库存',
    status           tinyint(1)     NOT NULL DEFAULT '0' COMMENT '状态 1:启用 2:停用',
    is_deleted       int(11)        NOT NULL DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    create_time      datetime       NOT NULL COMMENT '创建时间',
    update_time      datetime       NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY uk_name (name) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat商品表';

-- ----------------------------
-- Table structure for chat_room
-- ----------------------------
DROP TABLE IF EXISTS chat_room;
CREATE TABLE chat_room
(
    id                    bigint(20)    NOT NULL COMMENT 'ID',
    user_id               bigint(20)    NOT NULL COMMENT '用户 id#chat_user',
    ip                    varchar(255) DEFAULT NULL COMMENT 'ip',
    conversation_id       varchar(255) DEFAULT NULL COMMENT '对话 id',
    first_chat_message_id bigint(20)    NOT NULL COMMENT '第一条消息主键',
    first_message_id      varchar(255)  NOT NULL COMMENT '第一条消息 id',
    title                 varchar(5000) NOT NULL COMMENT '对话标题',
    api_type              varchar(20)   NOT NULL COMMENT 'API 类型',
    create_time           datetime      NOT NULL COMMENT '创建时间',
    update_time           datetime      NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY first_chat_message_id_uk (first_chat_message_id) USING BTREE,
    UNIQUE KEY first_message_id_uk (first_message_id) USING BTREE,
    UNIQUE KEY conversation_id_uk (conversation_id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='chat聊天室表';

-- ----------------------------
-- Table structure for chat_sensitive_word
-- ----------------------------
DROP TABLE IF EXISTS chat_sensitive_word;
CREATE TABLE chat_sensitive_word
(
    id          bigint(20)   NOT NULL COMMENT 'ID',
    word        varchar(255) NOT NULL COMMENT '敏感词',
    status      int(11)      NOT NULL COMMENT '状态 1 启用 2 停用',
    is_deleted  int(11) DEFAULT '0' COMMENT '是否删除 0 否 1 是',
    create_time datetime     NOT NULL COMMENT '创建时间',
    update_time datetime     NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat敏感词表';

-- ----------------------------
-- Table structure for chat_user
-- ----------------------------
DROP TABLE IF EXISTS chat_user;
CREATE TABLE chat_user
(
    id                     bigint(20)                        NOT NULL COMMENT '用户ID',
    nickname               varchar(16) CHARACTER SET utf8mb4 NOT NULL COMMENT '用户昵称',
    description            varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '描述',
    avatar_version         int(11)                           NOT NULL COMMENT '头像版本',
    last_login_ip          varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '上一次登录 IP',
    create_time            datetime                          NOT NULL COMMENT '创建时间',
    update_time            datetime                          NOT NULL COMMENT '更新时间',
    can_conversation_times int(11)                           DEFAULT '0' COMMENT '可聊天次数',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='Chat用户表';

-- ----------------------------
-- Table structure for chat_user_binding
-- ----------------------------
DROP TABLE IF EXISTS chat_user_binding;
CREATE TABLE chat_user_binding
(
    id                    bigint(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
    binding_type          varchar(16) NOT NULL COMMENT '绑定类型',
    user_binding_email_id bigint(20)  NOT NULL COMMENT '额外信息表的用户ID#chat_user_binding_email',
    user_id               bigint(20)  NOT NULL COMMENT '基础用户表的ID#chat_user',
    create_time           datetime    NOT NULL COMMENT '创建时间',
    update_time           datetime    NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY user_binding_email_id_uk (binding_type, user_id, user_binding_email_id) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1659950412171575297
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat用户绑定关系表';

-- ----------------------------
-- Table structure for chat_user_binding_email
-- ----------------------------
DROP TABLE IF EXISTS chat_user_binding_email;
CREATE TABLE chat_user_binding_email
(
    id          bigint(20)   NOT NULL COMMENT 'ID',
    username    varchar(64)  NOT NULL COMMENT '用户名',
    password    varchar(128) NOT NULL COMMENT '密码',
    verified    tinyint(4)   NOT NULL DEFAULT '0' COMMENT '是否验证过，false 否 true 是',
    create_time datetime     NOT NULL COMMENT '创建时间',
    update_time datetime     NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE,
    UNIQUE KEY username_pk (username) USING BTREE COMMENT '用户名uk'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat用户邮箱绑定表';

-- ----------------------------
-- Table structure for chat_user_exchange_record
-- ----------------------------
DROP TABLE IF EXISTS chat_user_exchange_record;
CREATE TABLE chat_user_exchange_record
(
    id          bigint(20)   NOT NULL COMMENT 'ID',
    user_id     bigint(20)   NOT NULL COMMENT '用户id#chat_user',
    exchange_id bigint(20)   NOT NULL COMMENT '兑换id#chat_exchange',
    code        varchar(100) NOT NULL COMMENT '通兑码#chat_exchange',
    create_time datetime     NOT NULL COMMENT '创建时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat用户聊天兑换记录表';

-- ----------------------------
-- Table structure for chat_user_login_log
-- ----------------------------
DROP TABLE IF EXISTS chat_user_login_log;
CREATE TABLE chat_user_login_log
(
    id                    bigint(20)  NOT NULL COMMENT 'ID',
    user_id               bigint(20)  NOT NULL COMMENT '登录的基础用户ID#chat_user',
    login_type            varchar(32) NOT NULL COMMENT '登录方式（注册方式），邮箱登录，手机登录等等',
    user_binding_email_id bigint(20)  NOT NULL COMMENT '登录信息ID与login_type有关联，邮箱登录时关联#chat_user_binding_email',
    login_ip              varchar(32) NOT NULL COMMENT '登录的IP地址',
    login_status          tinyint(1)  NOT NULL COMMENT '登录状态，1登录成功，0登录失败',
    message               varchar(64) NOT NULL COMMENT '登录后返回的消息',
    create_time           datetime    NOT NULL COMMENT '创建时间',
    PRIMARY KEY (id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Chat用户登录日志表';

-- ----------------------------
-- Table structure for sys_email_send_log
-- ----------------------------
DROP TABLE IF EXISTS sys_email_send_log;
CREATE TABLE sys_email_send_log
(
    id                 bigint(20)    NOT NULL COMMENT 'ID',
    from_email_address varchar(64)   NOT NULL COMMENT '发件人邮箱',
    to_email_address   varchar(64)   NOT NULL COMMENT '收件人邮箱',
    biz_type           int(11)       NOT NULL COMMENT '业务类型(10:用户注册验证码认证 11:用户找回密码验证码认证 12:兑换码购买)',
    request_ip         varchar(255)  NOT NULL COMMENT '请求 ip',
    content            text          NOT NULL COMMENT '发送内容',
    message_id         varchar(128)  NOT NULL COMMENT '消息id',
    status             tinyint(4)    NOT NULL COMMENT '发送状态，1成功，0失败',
    message            varchar(1024) NOT NULL COMMENT '发送后的消息，用于记录成功/失败的信息，成功默认为 success',
    create_time        datetime      NOT NULL COMMENT '创建时间',
    update_time        datetime      NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Sys系统邮箱发送日志表';

-- ----------------------------
-- Table structure for sys_email_verify_code
-- ----------------------------
DROP TABLE IF EXISTS sys_email_verify_code;
CREATE TABLE sys_email_verify_code
(
    id               bigint(20)   NOT NULL COMMENT 'ID',
    to_email_address varchar(64)  NOT NULL COMMENT '验证码接收邮箱地址',
    verify_code      varchar(32)  NOT NULL COMMENT '验证码唯一',
    is_used          tinyint(1)   NOT NULL COMMENT '是否使用，false 否，true 是',
    verify_ip        varchar(100) NOT NULL COMMENT '核销IP，方便识别一些机器人账号',
    expire_at        datetime     NOT NULL COMMENT '验证码过期时间',
    biz_type         tinyint(4)   NOT NULL COMMENT '当前邮箱业务',
    create_time      datetime     NOT NULL COMMENT '创建时间',
    update_time      datetime     NOT NULL COMMENT '更新时间',
    PRIMARY KEY (id, is_used) USING BTREE,
    UNIQUE KEY verify_code_uk (verify_code) USING BTREE COMMENT '验证码唯一uk'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  ROW_FORMAT = DYNAMIC COMMENT ='Sys系统邮箱验证码表';

SET FOREIGN_KEY_CHECKS = 1;
